package clinicauna.util;

import clinicauna.model.ArchivoDto;
import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Base64;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

public class ConverArchivo {

    /**
     * Recibe el string de la imagen y lo pasa a archivo Image
     *
     * @param imageString String de la imagen
     * @return retorna el archivo image
     */
    public static BufferedImage stringToImage(String imageString) {
        Image im;

        BufferedImage image = null;
        byte[] imageByte;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    public static String guardarImagen(String im, ArchivoDto archivo) throws IOException {
        String path = "Archivos/" + archivo.getExpediente().getId().toString() + archivo.getFecha().toString() + archivo.getNombreExamen() + "." + archivo.getTipo();

        BufferedImage imagen = stringToImage(im);
        File file = new File(path);
        ImageIO.write((RenderedImage) imagen, archivo.getTipo(), file);

        return path;
    }

    /**
     * Pasa del string de la imagen al bufer de la misma
     *
     * @param imageString
     * @return
     */
    public static BufferedImage stringToBufferedImage(String imageString) {

        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);;
        byte[] imageByte;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Pasa de imagen a String
     *
     * @param im recibe una Image
     * @param tipoImage Tipo de imagen
     * @return El string de la imagen
     */
    public static String imageToString(BufferedImage im, String tipoImage) {

        BufferedImage image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
        //pasa de imagen a buffer o al reves
        image = im;//SwingFXUtils.fromFXImage(im, null);

        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, tipoImage, bos);
            byte[] imageBytes = bos.toByteArray();

            Base64.Encoder encode = Base64.getEncoder();
            imageString = encode.encodeToString(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

    public static String fileToString(String im) throws URISyntaxException {
        Base64.Encoder encode = Base64.getEncoder();
        try {
            File file = new File(new URI(im));
            byte[] fileArray = new byte[(int) file.length()];
            String encodedFile = "";
            InputStream inputStream;
            inputStream = new FileInputStream(file);
            inputStream.read(fileArray);
            encodedFile = encode.encodeToString(fileArray);

            return encodedFile;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static void eliminarFile(String im) throws URISyntaxException {
        Base64.Encoder encode = Base64.getEncoder();
        try {
            File file = new File(new URI(im));
            file.delete();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /*public static String direccionArchivo() {
        String dre = ArchivoController.class.getSimpleName() + ".class";
        String rutaca = ArchivoController.class.getResource(dre).toString();
        rutaca = rutaca.replace("/", "\\\\");
        rutaca = rutaca.replace("file:", "");
        rutaca = rutaca.replace("\\\\C", "C");
        rutaca = rutaca.replace("\\\\build", "");
        rutaca = rutaca.replace("\\\\web", "");
        rutaca = rutaca.replace("\\\\WEB-INF", "");
        rutaca = rutaca.replace("\\\\classes", "");
        rutaca = rutaca.replace("\\\\controller", "");
        rutaca = rutaca.replace("\\\\ArchivoController.class", "");
        rutaca = rutaca.replace("\\\\WsClinicaUNA", "\\\\");
        rutaca = rutaca.replace("%20", "");
        rutaca = rutaca + "WsClinicaUNA\\Archivos\\";
        //System.out.println("ruta: "+rutaca);
        return rutaca;
    }*/
    public static String stringToFile(String arCode, ArchivoDto archivo) {
        try {
            byte[] imageByte;
            Base64.Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(arCode);

            String fileName = "Archivos/" + archivo.getExpediente().getId().toString() + archivo.getFecha().toString() + archivo.getNombreExamen() + ".pdf";//direccionArchivo() + archivo.getExpediente().getId().toString() + archivo.getFecha().toString() + archivo.getNombreExamen() + ".pdf";

            OutputStream out = new FileOutputStream(fileName);

            out.write(imageByte);
            out.close();

            return fileName;
        } catch (Exception n) {
            System.out.println(n);
            return null;
        }
    }

    public void mostrarFile(String result) {
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            File file = new File("Reporte.pdf");
            byte[] fileArray = decoder.decode(result);
            OutputStream put = new FileOutputStream(file);
            put.write(fileArray);
            put.close();
            InputStream input = new FileInputStream(file);
            input.read(fileArray);
            //JasperViewer jv = new JasperViewer(input, true);
            input.close();
            Desktop.getDesktop().open(file);
        } catch (Exception e) {
        }
    }
}
