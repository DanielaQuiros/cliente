/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author Dani
 */
public class ContrasenaRandom 
{
    private String[] caracteres= {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    private int longitud;
    private String contra;
    
    public ContrasenaRandom()
    {
      longitud=8;
    }
    	public String generarContrasena()
	{
		contra="";
		try
		{
			Random random = SecureRandom.getInstanceStrong();
			StringBuilder sb = new StringBuilder(longitud);
			
			for (int i = 0; i < longitud; i++) 
			{
				int indexRandom = random.nextInt( caracteres.length );
				sb.append( caracteres[indexRandom] );
			}
			 contra = sb.toString();
		}
		catch(NoSuchAlgorithmException ex)
		{
			
		}
		return contra;
	}
    
}
