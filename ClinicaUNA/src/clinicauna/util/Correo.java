/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.util;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Correo 
{
    Properties props;
    Session session;
    Message message;
    Transport transport;
    BodyPart messageBodyPart;
    Multipart multipart;
    DataSource source;
    String usuario; 
    String contra; 
		
    public Correo(String usuario, String contra)
    {
        props=new Properties();
        this.usuario = usuario;
        this.contra = contra;
        props.put("mail.smtp.host","smtp.gmail.com");
        props.put("mail.smtp.user",this.usuario);
        props.put("mail.smtp.auth","true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.port","587");
        
        session = Session.getDefaultInstance( props);
    }
    public void Enviar(String destinatario,String asunto ,String ruta , String nombre) 
    { 
        try
        {
            message=new MimeMessage(session);
            multipart.addBodyPart(messageBodyPart);
            messageBodyPart=new MimeBodyPart();
            source= new FileDataSource(ruta);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(nombre);
            multipart=new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);


            message.setFrom(new InternetAddress(this.usuario));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(destinatario));
            message.setSubject(asunto);
            message.setContent(multipart,"text/html");
            transport=session.getTransport("smtp");
            transport.connect(this.usuario,this.contra);
            transport.sendMessage(message,message.getAllRecipients());
            transport.close();
    }
    catch(MessagingException e)
    {
        System.out.println(e.toString());
    }
}
    public void EnviarTexto(String  destino, String asunto,String mensaje)
    {
        try
        {
            message=new MimeMessage(session);
            message.setFrom(new InternetAddress(this.usuario));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(destino));
            message.setSubject(asunto);
            message.setContent(mensaje,"text/html");
            transport=session.getTransport("smtp");
            transport.connect(this.usuario,this.contra);
            transport.sendMessage(message,message.getAllRecipients());
            transport.close();

        }
        catch(MessagingException e)
        {
            System.out.println(e.toString());
        }
    }
	
}
