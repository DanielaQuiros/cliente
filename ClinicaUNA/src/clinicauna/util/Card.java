/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.util;

import clinicauna.model.CitaDto;
import java.util.ResourceBundle;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author liedu
 */
public class Card extends Button {

    private Integer ancho;
    private Integer alto;
    private ImageView fondo;
    // no tiene aun
    private Image libre = new Image("/clinicauna/resources/eLibre.png");
    //programada "P"
    private Image ocupada = new Image("/clinicauna/resources/eProgramada.png");
    //atendida "A"
    private Image atendida = new Image("/clinicauna/resources/eAtendido.png");
    //No se presento "N"
    private Image ausente = new Image("/clinicauna/resources/eAusente.png");

    private String Dia;
    private String Hora;
    private String Paciente;
    private CitaDto cita;
    ResourceBundle idioma;

    public Card() {
        idioma = FlowController.getInstance().getLanguage();
    }

    public void iniciar() {
        fondo = new ImageView();
        actualizar();
        fondo.setFitHeight(35);
        fondo.setFitWidth(30);
    }

    public CitaDto getCita() {
        return cita;
    }

    public void setCita(CitaDto cita) {
        this.cita = cita;
    }

    public ImageView getFondo() {
        return fondo;
    }

    public void setFondo(ImageView fondo) {
        this.fondo = fondo;
    }

    /**
     * Asigana en el lebel el texto correspondiente
     *
     * @param cit
     */
    public void text() {
        Dia = String.valueOf(this.cita.getCitFecha().getDayOfMonth()) + "-" + String.valueOf(this.cita.getCitFecha().getMonthValue());
        Hora = this.cita.getCitHora();
        if (this.cita.getPaciente().getPacNombre() != null) {
            if (this.cita.getCitEstado().equals("C")) {
                Paciente = "Cancelada";
            } else {
                Paciente = this.cita.getPaciente().getPacNombre();
            }
        } else {
            Paciente = idioma.getString("libre");
        }
        this.setAlignment(Pos.CENTER);
        this.setText(idioma.getString("dia") + Dia.replace("-", "/") + "\n"
                + idioma.getString("hora") + Hora + "\n"
                + Paciente);
    }

    /**
     * Asigna el tamaño de los label
     *
     * @param width
     * @param height
     *
     */
    public void tamano(int width, int height) {
        ancho = width;
        alto = height;
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
    }

    /**
     * Define el color que tendra
     *
     */
    public void Style() {
        this.setGraphicTextGap(5);
        this.setContentDisplay(ContentDisplay.RIGHT);
        iniciar();
        this.setGraphic(fondo);
        tamano(150, 75);
        text();
    }

    public void actualizar() {
        switch (this.cita.getCitEstado()) {
            case "A":
                fondo.setImage(atendida);
                break;
            case "P":
                fondo.setImage(ocupada);
                break;
            case "C":
                fondo.setImage(libre);
                break;
            case "N":
                fondo.setImage(ausente);
                break;
            default:
                fondo.setImage(libre);
        }
    }

    /**
     * Pantalla donde ira al dar click
     *
     * @param pantalla
     * @param stage
     *
     */
    public void go(String pantalla, Stage stage) {
        this.setOnMouseClicked((event) -> {
            AppContext.getInstance().set("citaClick", null);
            Card c = (Card) event.getSource();
            AppContext.getInstance().set("citaClick", c.getCita());
            FlowController.getInstance().goView(pantalla);
        });
    }
}
