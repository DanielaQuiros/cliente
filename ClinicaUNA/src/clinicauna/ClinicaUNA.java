/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna;

import clinicauna.util.FlowController;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Susana
 */
public class ClinicaUNA extends Application {
    
    @Override
    public void start(Stage stage) 
    {
        FlowController.getInstance().InitializeFlow(stage,ResourceBundle.getBundle("clinicauna/resources/i18n/Espanol"));
        stage.getIcons().add(new Image("clinicauna/resources/proyecto.jpg"));
        stage.setTitle("Clinica UNA");
        stage.setResizable(false);
        FlowController.getInstance().goViewInWindow("LogIn");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
