/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.UsuarioDto;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class MenuDoctController extends Controller implements Initializable {

    @FXML
    private JFXButton btnPacientes;
    @FXML
    private JFXButton btnUsuarios;
    @FXML
    private JFXButton btnCitas;
    @FXML
    private JFXButton btnExpedientes;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
 
    }    

    @Override
    public void initialize() {

    }

    @FXML
    private void evtPacientes(ActionEvent event) {
        FlowController.getInstance().goView("RegPacientes");
    }

    @FXML
    private void evtUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("MantUsuarios");
    }

    @FXML
    private void evtCitas(ActionEvent event) {
        AppContext.getInstance().set("vengo", "menu");
        FlowController.getInstance().goView("Agenda");
    }

    @FXML
    private void evtExpedientes(ActionEvent event) {
        FlowController.getInstance().goView("MantExpedientes");
        MantExpedientesController ex = (MantExpedientesController) AppContext.getInstance().get("expController");
        ex.limpiar();
    }

    @FXML
    private void btnReportes(ActionEvent event) {
        FlowController.getInstance().goView("Reporte");
    }
    
}
