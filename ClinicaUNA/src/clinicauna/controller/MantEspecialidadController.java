/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.EspecialidadDto;
import clinicauna.model.MedicoDto;
import clinicauna.service.EspecialidadService;
import clinicauna.service.MedicoService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class MantEspecialidadController extends Controller implements Initializable 
{

    @FXML
    private JFXTextField tfNombre;
    @FXML
    private TableView<EspecialidadDto> tbEspecialidad;
    @FXML
    private TableColumn<EspecialidadDto,String> tcNombre;
    
    EspecialidadDto espDto;
    MedicoDto medDto;
    EspecialidadService espService;
    boolean buscar,buscarM,eliminar;
    List<Node> requeridos = new ArrayList<>();
    ObservableList <EspecialidadDto> esp;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
       espService = new EspecialidadService();
       tcNombre.setCellValueFactory(x->new SimpleStringProperty(x.getValue().espNombre.get()));
       Respuesta respuesta = espService.getEspecialidades();
       if(respuesta.getEstado())
       {
            esp = FXCollections.observableArrayList((List<EspecialidadDto>)respuesta.getResultado("Especialidades"));
            tbEspecialidad.setItems(esp);
       }
       
    }    

    @Override
    public void initialize() 
    {
       espDto = new EspecialidadDto();
       medDto = new MedicoDto();
       espService = new EspecialidadService();
       buscar = false;
       buscarM = false; 
       Respuesta respuesta = espService.getEspecialidades();
       if(respuesta.getEstado())
       {
            esp = FXCollections.observableArrayList((List<EspecialidadDto>)respuesta.getResultado("Especialidades"));
            tbEspecialidad.setItems(esp);
       }
       
    }
    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            }
            
        }
        if (validos) {
            return "";
        } else {
            return FlowController.getInstance().getLanguage().getString("camposRequeridos") + " [" + invalidos + "].";
        }
    }
    public void cargarEspecialidad()
    {
        if(buscar && AppContext.getInstance().get("EspB") != null)
        {
            EspecialidadDto espB= (EspecialidadDto)AppContext.getInstance().get("EspB");
            espDto = espB;
            tfNombre.textProperty().bindBidirectional(espDto.espNombre);
        }
        buscar = false;
    }
    @FXML
    private void evtBuscar(ActionEvent event) 
    {
       FlowController.getInstance().goViewInWindowModal("buscarEspecialidad", getStage(), false);
       buscar = true;
       cargarEspecialidad();
    }

    @FXML
    private void evtGuardar(ActionEvent event) 
    {
        if(!tfNombre.getText().isEmpty())
        {
            espDto.setEspNombre(tfNombre.getText());
            Respuesta respuesta = espService.guardarEspecialidad(espDto);
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "", getStage(), FlowController.getInstance().getLanguage().getString("msjEsp"));
            esp.add(espDto);
            tbEspecialidad.refresh();
            tfNombre.clear();
        }
        else 
        {
            new Mensaje().showModal(Alert.AlertType.ERROR, "", getStage(), FlowController.getInstance().getLanguage().getString("msjErrorEsp"));
        }
    }

    @FXML
    private void evtEliminar(ActionEvent event) 
    {
        try
        {
            espDto = (EspecialidadDto)AppContext.getInstance().get("EspR");
            Respuesta res = espService.eliminarEspecialidad(espDto.getEspId());
            if(!res.getEstado())
            {
                new Mensaje().showModal(Alert.AlertType.ERROR,"",getStage(),FlowController.getInstance().getLanguage().getString("msjErrorEliminar"));
            }
        }
        catch(Exception ex)
        {
            System.out.println("Error "+ex.getMessage());
        }
    }

    @FXML
    private void evtLimpiar(ActionEvent event) 
    {
        tfNombre.clear();
    }

    @FXML
    private void evt_Especialidad(MouseEvent event) {
        EspecialidadDto esp = tbEspecialidad.getSelectionModel().getSelectedItem();
        Respuesta r = espService.getEspecialidad(esp.getEspNombre());
        List<EspecialidadDto> v = (List<EspecialidadDto>)r.getResultado("Especialidad");
        EspecialidadDto e = v.get(0);
        AppContext.getInstance().set("EspR",e);
    }



   
}
