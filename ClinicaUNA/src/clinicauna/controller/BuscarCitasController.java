/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.CitaDto;
import clinicauna.model.MedicoDto;
import clinicauna.service.CitaService;
import clinicauna.util.AppContext;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class BuscarCitasController extends Controller implements Initializable {

    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private JFXTextField txtPaciente;
    @FXML
    private JFXDatePicker dpFecha;
    @FXML
    private JFXTextField txtMedico;
    @FXML
    private TableView<CitaDto> tvCitas;
    @FXML
    private TableColumn<CitaDto, String> tcPaciente;
    @FXML
    private TableColumn<CitaDto, String> tcFecha;
    @FXML
    private TableColumn<CitaDto, String> tcHora;
    @FXML
    private TableColumn<CitaDto, String> tcMedico;

    private ObservableList<CitaDto> listCitas;
    private CitaDto cita;
    ResourceBundle idioma;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tcPaciente.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getPaciente().getPacNombre()));
        tcMedico.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getCitMedId().getMedUsuId().getUsuNombre()));
        tcFecha.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getCitFecha().toString()));
        tcHora.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getCitHora()));
    }

    @Override
    public void initialize() {
        cita = new CitaDto();
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        if (dpFecha.getValue() != null) {
            Respuesta resp = new CitaService().citas(txtMedico.getText(), dpFecha.getValue().toString(), txtPaciente.getText());
            if (resp.getEstado()) {
                List<CitaDto> citas = (List<CitaDto>) resp.getResultado("citas");
                listCitas = FXCollections.observableArrayList(citas);
                tvCitas.setItems(listCitas);
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Busqueda", getStage(), idioma.getString("msjBusqueda"));
        }
    }

    @FXML
    private void evtAceptar(ActionEvent event) {
        if (cita.getCitId() != null) {
            AppContext.getInstance().set("citaBusqueda", cita);
            getStage().close();
        }
    }

    @FXML
    private void evtTcitas(MouseEvent event) {
        cita = tvCitas.getSelectionModel().getSelectedItem();

    }

}
