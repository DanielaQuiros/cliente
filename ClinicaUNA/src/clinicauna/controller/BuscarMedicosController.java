/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.MedicoDto;
import clinicauna.service.MedicoService;
import clinicauna.util.AppContext;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class BuscarMedicosController extends Controller implements Initializable {

    @FXML
    private JFXTextField tf_Codigo;
    @FXML
    private JFXTextField tf_Folio;
    @FXML
    private JFXTextField tf_Carne;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private TableView<MedicoDto> tbMedicos;
    @FXML
    private TableColumn<MedicoDto, String> tcCodigo;
    @FXML
    private TableColumn<MedicoDto,String> tcFolio;
    @FXML
    private TableColumn<MedicoDto,String> tcCarne;
    @FXML
    private TableColumn<MedicoDto,String> tcNombre;
    
    ObservableList<MedicoDto> medicos;
    private MedicoService medService;
    private MedicoDto medicoDto;

    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        limpiar();
    }
    
    public void limpiar(){
        tf_Carne.clear();
        tf_Codigo.clear();
        tf_Folio.clear();
        //tf_Especialidad.clear();
        
        tbMedicos.getItems().clear();
    }

    @Override
    public void initialize() 
    {
        medService = new MedicoService();
        
        tcFolio.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getMedFolio()));
        tcCarne.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getMedCarne()));
        tcCodigo.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getMedCodigo()));
        tcNombre.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getMedUsuId().getUsuNombre()));
    }

    @FXML
    private void evtBuscar(ActionEvent event) 
    {
        tbMedicos.getItems().clear();
        Respuesta res = medService.getMedico(tf_Folio.getText().isEmpty() ? "%" :tf_Folio.getText(),tf_Carne.getText().isEmpty() ? "%" :tf_Carne.getText(),tf_Codigo.getText().isEmpty() ? "%" :tf_Codigo.getText());
        System.out.println(res);
        if(res.getEstado())
        {
             medicos= FXCollections.observableArrayList((List<MedicoDto>)res.getResultado("Medicos"));

             tbMedicos.setItems(medicos);
        }
        else
        {
            System.out.println(res.getMensaje());
        }
    }

    @FXML
    private void evtAceptar(ActionEvent event) 
    {
        if(tbMedicos.getSelectionModel().getSelectedItem() != null)
        {
            MedicoDto usuario= tbMedicos.getSelectionModel().getSelectedItem();
            AppContext.getInstance().set("medico",usuario);
            getStage().close();
        }
    }

}
