/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.CitaDto;
import clinicauna.model.ConsultaDto;
import clinicauna.model.ExpedienteDto;
import clinicauna.model.PacienteDto;
import clinicauna.model.UsuarioDto;
import clinicauna.service.ArchivoService;
import clinicauna.service.CitaService;
import clinicauna.service.ConsultaService;
import clinicauna.service.ExpedienteService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class HistorialConsultasController extends Controller implements Initializable {

    @FXML
    private TableView<ConsultaDto> tblConsultas;
    @FXML
    private TableColumn<ConsultaDto, String> cdFecha;
    @FXML
    private TableColumn<ConsultaDto, String> tcHora;
    @FXML
    private TableColumn<ConsultaDto, String> tcMotivo;
    @FXML
    private LineChart<String, Number> graficoImc;
    @FXML
    private Label lblPaciente;
    
    private PacienteDto paciente;
    private ExpedienteDto expediente;
    private ExpedienteService expS;
    private ObservableList<ConsultaDto> consultas;
    private ConsultaService conS;
    List<LocalTime> listHras;
    @FXML
    private JFXButton btnAgregarConsulta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        expS = new ExpedienteService();
        conS = new ConsultaService();
        consultas = FXCollections.observableArrayList();
        
        tcHora.setCellValueFactory(x->x.getValue().cita.citHora.asString());
        cdFecha.setCellValueFactory(x->x.getValue().cita.citFecha.asString());
        tcMotivo.setCellValueFactory(x->x.getValue().cita.citMotivo);
        
    }    

    @Override
    public void initialize() {
        paciente = new PacienteDto();
        expediente = new ExpedienteDto();
        
        limpiar();
        
        paciente = (PacienteDto) AppContext.getInstance().get("buscarPaciente");
        Respuesta res = expS.getExpediente(paciente.getPacId());
        expediente = (ExpedienteDto) res.getResultado("expediente");
        
        lblPaciente.setText(FlowController.getInstance().getLanguage().getString("paciente") + ": "+ paciente.getPacNombre() + " " +paciente.getPacApellidos());
        
        Respuesta respuesta = conS.getConsultas(expediente.id);
        if(respuesta.getEstado()){
            consultas = (FXCollections.observableArrayList((List<ConsultaDto>)respuesta.getResultado("Consultas")));
            tblConsultas.setItems(consultas);
        }else{
        
        }
        
        final NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel(FlowController.getInstance().getLanguage().getString("pacEvolucion"));
        graficoImc.setTitle(FlowController.getInstance().getLanguage().getString("pacEvolucion"));
        XYChart.Series series = new XYChart.Series();
        series.setName(FlowController.getInstance().getLanguage().getString("imc"));
        if(!consultas.isEmpty()){
            consultas.forEach((consulta) -> {
                series.getData().add(new XYChart.Data(consulta.getCita().getCitFecha().toString(), consulta.getConImc()));
            });
        }
        graficoImc.getData().add(series);
        
        UsuarioDto usuario = new UsuarioDto();
        usuario = (UsuarioDto) AppContext.getInstance().get("usuarioActual");
        if(usuario.getUsuTipo().equals("M")){
            btnAgregarConsulta.setVisible(true);
            btnAgregarConsulta.setDisable(false);
        }else{
            btnAgregarConsulta.setVisible(false);
            btnAgregarConsulta.setDisable(true);
        }
    }

    @FXML
    private void evtAtras(ActionEvent event) {
        FlowController.getInstance().goView("MantExpedientes");
    }

    @FXML
    private void evtTblConsultas(MouseEvent event) {
        if(tblConsultas.getSelectionModel().getSelectedItem() != null){
            AppContext.getInstance().set("consultaActual", tblConsultas.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    private void btnAgregarConsulta(ActionEvent event) {
       AppContext.getInstance().set("expedienteActual", expediente);
       AppContext.getInstance().set("consultaActual", null);
       FlowController.getInstance().goViewInWindowModal("agregarCita", getStage(), false);
       if(AppContext.getInstance().get("citaActual") != null){
           FlowController.getInstance().goView("MantConsulta");
       }
    }

    @FXML
    private void btnVerConsulta(ActionEvent event) {
        if(AppContext.getInstance().get("consultaActual") != null){
            AppContext.getInstance().set("expediente", "expediente");
            FlowController.getInstance().goView("MantConsulta");
        }else 
            new Mensaje().show(Alert.AlertType.WARNING, "", FlowController.getInstance().getLanguage().getString("mjsSelecConsulta"));
    }
    
    public void limpiar(){
        consultas.clear();
        tblConsultas.getItems().clear();
        graficoImc.getData().clear();
    }
    
}
