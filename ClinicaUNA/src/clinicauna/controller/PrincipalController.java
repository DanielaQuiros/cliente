/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class PrincipalController extends Controller implements Initializable {

    @FXML
    private BorderPane root;
    @FXML
    private JFXButton btnHam;
    @FXML
    private JFXDrawer drawer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //abrirMenu("BarraM");
        btnHam.setDisable(false);
        AppContext.getInstance().set("boton",btnHam);
        AppContext.getInstance().set("Principal", this);
    }

    @Override
    public void initialize() {

    }

    public void abrirMenu(String menu) {
        try {
            HBox hb = FXMLLoader.load(getClass().getResource("/clinicauna/view/" + menu + ".fxml"), FlowController.getInstance().getLanguage());
            this.drawer.setSidePane(hb);
            drawer.setDirection(JFXDrawer.DrawerDirection.TOP);
        } catch (IOException ex) {
            Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.btnHam.setOnMouseClicked((MouseEvent e) -> {
            if (this.drawer.isShown()) {
                this.drawer.close();
            } else {
                this.drawer.open();
            }
            e.consume();
        });
    }
}
