
package clinicauna.controller;

import clinicauna.model.UsuarioDto;
import clinicauna.service.UsuarioService;
import clinicauna.util.AppContext;
import clinicauna.util.Correo;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class MantUsuariosController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfNombre;
    @FXML
    private JFXTextField tfPApellido;
    @FXML
    private JFXTextField tfSApellido;
    @FXML
    private JFXTextField tfCedula;
    @FXML
    private JFXTextField tfCorreo;
    @FXML
    private JFXCheckBox cbEstado;
    @FXML
    private JFXComboBox<String> cmbTipo;
    @FXML
    private JFXTextField tfUsuario;
    @FXML
    private JFXTextField tfContra;
    @FXML
    private JFXComboBox<String> cmbIdioma;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnBuscar;
    
    UsuarioDto usuario, usu, usuarioActual;
    UsuarioService usuarioService;
    String enlace="http://localhost:8000/WsClinicaUNA/ws/UsuarioController/activarUsuario/";
    String usuId;
    Correo correo;
    boolean modificado;
    List<Node> requeridos = new ArrayList<>();
    boolean buscar;
    Long ua;
    UsuarioService us = new UsuarioService();

    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        usuarioService = new UsuarioService();
        usuario= new UsuarioDto();
        cmbTipo.getItems().addAll(
                FlowController.getInstance().getLanguage().getString("admin"), //0
                FlowController.getInstance().getLanguage().getString("recep"), //1
                FlowController.getInstance().getLanguage().getString("doc")     //2
        );
        cmbIdioma.getItems().addAll(
                FlowController.getInstance().getLanguage().getString("esp"),
                FlowController.getInstance().getLanguage().getString("ing")
        );
    }
    @Override
    public void initialize() 
    {
        cbEstado.setDisable(true);
        correo = new Correo("unaprogra32018@gmail.com","una32018");
        usuId="";
        buscar = false;
        modificado = false;
        agregarRequeridos();
        nuevoUsuario();
        Limpiar();
        
        ua = ((UsuarioDto) AppContext.getInstance().get("usuarioActual")).getUsuId();
        Respuesta res = new UsuarioService().getUsuId(ua);
        usuarioActual = (UsuarioDto) res.getResultado("Usuario");
        ua = usuarioActual.getUsuId();
        if(usuarioActual.getUsuTipo().equals("A")){
            btnEliminar.setVisible(true);
            btnEliminar.setDisable(false);
            cmbTipo.setDisable(false);
            requeridos.add(cmbTipo);
        }else{
            btnEliminar.setVisible(false);
            btnEliminar.setDisable(true);
            cmbTipo.setDisable(true);
        }
    }
    public void agregarRequeridos()
    {     
        requeridos.clear();
        requeridos.addAll(Arrays.asList(tfNombre, tfPApellido,tfSApellido, tfCedula, tfCorreo,tfUsuario,tfContra,cbEstado,cmbIdioma));//,cmbTipo));
    }
    public String validarRequeridos(){
        Boolean validos = true; 
        String invalidos = "";
        for(Node node : requeridos){
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) 
        {
            return "";
        } 
        else 
        {
            return FlowController.getInstance().getLanguage().getString("camposRequeridos")+" [" + invalidos + "].";
        }
    }
    private void bindUsuario()
    {
        tfNombre.textProperty().bindBidirectional(usuario.usuNombre);
        tfCedula.textProperty().bindBidirectional(usuario.usuCedula);
        tfPApellido.textProperty().bindBidirectional(usuario.usuPapellido);
        tfSApellido.textProperty().bindBidirectional(usuario.usuSapellido);
        tfUsuario.textProperty().bindBidirectional(usuario.usuUsuario);
        tfContra.textProperty().bindBidirectional(usuario.usuContrasena);
        tfCorreo.textProperty().bindBidirectional(usuario.usuCorreo);
        if(usuario.usuTipo != null)
        {
            switch (cmbTipo.getSelectionModel().getSelectedIndex()) 
            {
                case 0:
                    usuario.setUsuTipo("A");
                    break;
                case 1:
                    usuario.setUsuTipo("M");
                    break;
                case 2:
                    usuario.setUsuTipo("R");
                    break;
                default:
                    break;
            }
        }
        if(cmbIdioma.getSelectionModel().getSelectedIndex()==0)
        {
            usuario.setUsuIdioma("I");
        }
        else
        {
            usuario.setUsuIdioma("E");
        }
    }
    private void unbindUsuario()
    {
        tfNombre.textProperty().unbindBidirectional(usuario.usuNombre);
        tfCedula.textProperty().unbindBidirectional(usuario.usuCedula);
        tfPApellido.textProperty().unbindBidirectional(usuario.usuPapellido);
        tfSApellido.textProperty().unbindBidirectional(usuario.usuSapellido);
        tfUsuario.textProperty().unbindBidirectional(usuario.usuUsuario);
        tfContra.textProperty().unbindBidirectional(usuario.usuContrasena);
        tfCorreo.textProperty().unbindBidirectional(usuario.usuCorreo);
        cmbIdioma.getSelectionModel().clearSelection();
        cmbTipo.getSelectionModel().clearSelection();
        
    }
    private void nuevoUsuario()
    {
        unbindUsuario();
        usuario= new UsuarioDto();
        bindUsuario();
        
    }
    public void cargarUsuario()
    {
        if(buscar && AppContext.getInstance().get("usuarioB") != null)
        {
            Limpiar();
            usuario = (UsuarioDto) AppContext.getInstance().get("usuarioB");
            bindUsuario();
        
            switch (usuario.getUsuTipo()) 
            {
                case "A":
                    cmbTipo.getSelectionModel().select(0);
                    break;
                case "M":
                    cmbTipo.getSelectionModel().select(2);
                    break;
                case "R":
                    cmbTipo.getSelectionModel().select(1);
                    break;
                default:
                    break;
            }
        //}
        if(usuario.getUsuIdioma().equals("I"))
        {
            cmbIdioma.getSelectionModel().select(1);
        }
        else
        {
            cmbIdioma.getSelectionModel().select(0);
        }
        }
        buscar = false;
    }
    @FXML
    private void evtGuardar(ActionEvent event) 
    {
    boolean m = false;
    usuarioActual = (UsuarioDto)AppContext.getInstance().get("usuarioActual");
    String validos= validarRequeridos();
    if(!validos.isEmpty())
    {
        new Mensaje().showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("msjCUsu"), getStage(),validos);
    }
    else
    {
        Respuesta r= usuarioService.validarUsuario(tfUsuario.getText());
        UsuarioDto p = (UsuarioDto)r.getResultado("Usuario");
        if(usuario.getUsuId() != null && p != null && p.getUsuId() == ua && p.getUsuUsuario().equalsIgnoreCase(usuario.getUsuUsuario().trim()))
        {
           m = true;
           bindUsuario();
           Respuesta respuesta = usuarioService.guardarUsuario(usuario);
           //Limpiar();
           new Mensaje().showModal(Alert.AlertType.INFORMATION,"",getStage(),FlowController.getInstance().getLanguage().getString("msjUMod"));

        }
        else
        {
            if(p != null && p.getUsuId() != null && p.getUsuId() != usuario.getUsuId()){
                new Mensaje().showModal(Alert.AlertType.INFORMATION,"",getStage(),FlowController.getInstance().getLanguage().getString("mjsUsuRepetido"));
                }else{
                    usuario.setUsuNombre(tfNombre.getText());
                    usuario.setUsuCedula(tfCedula.getText());
                    usuario.setUsuPapellido(tfPApellido.getText());
                    usuario.setUsuSapellido(tfSApellido.getText());
                    usuario.setUsuUsuario(tfUsuario.getText());
                    usuario.setUsuContrasena(tfContra.getText());
                    usuario.setUsuCorreo(tfCorreo.getText());
                    if(cmbTipo.getValue() != null)
                    {
                        switch (cmbTipo.getSelectionModel().getSelectedIndex()) 
                        {
                            case 0:
                                usuario.setUsuTipo("A");
                                break;
                            case 1:
                                usuario.setUsuTipo("R");
                                break;
                            case 2:
                                usuario.setUsuTipo("M");
                                break;
                            default:
                                break;
                        }
                    }else{
                        if(cmbTipo.isDisable())
                            usuario.setUsuTipo("R");
                    }
                    if(cmbIdioma.getSelectionModel().getSelectedIndex()==0)
                    {
                        usuario.setUsuIdioma("E");
                    }
                    else
                    {
                        usuario.setUsuIdioma("I");
                    }
                    Respuesta respuesta = usuarioService.guardarUsuario(usuario);
                    usuario=(UsuarioDto)respuesta.getResultado("Usuario");
                        if(!respuesta.getEstado())
                        {
                            new Mensaje().showModal(Alert.AlertType.ERROR,"",getStage(),FlowController.getInstance().getLanguage().getString("msjEsUsu"));
                    }else{
                        if(!m)
                            enviarCorreo();
                        new Mensaje().showModal(Alert.AlertType.INFORMATION,"",getStage(),FlowController.getInstance().getLanguage().getString("msjGUsu"));
                    }
                }
            }
        }
              
    }

    
private void enviarCorreo(){
    try
    {
    Respuesta resp = usuarioService.getUsuario(usuario.getUsuUsuario(),usuario.getUsuContrasena());
        usu=(UsuarioDto)resp.getResultado("Usuario");
        usuId=usu.getUsuId().toString();
        if(!usu.getUsuEstado().equals("A"))
        {
            Thread thread = new Thread(()->
            {
                correo.EnviarTexto(tfCorreo.getText(),"Enlace de activacion de usuario", 
                "<table style=\"max-width:600px;padding:10px;margin:0 auto;border-collapse:collapse\">"+
                "<tbody><tr>"+
                "<td style=\"background-color:#E1F5FE\">"+
                "<div style=\"color:#34495e;margin:4% 10% 2%;text-align:justify;font-family:candara\">"+
                "<h2 style=\"color:#03366;margin:0 0 7px;font-family:candara\">Link de activacion de usuario</h2>"+
                "<p style=\"margin:2px;font-size:15px;font-family:roboto\">"+
                "Para activar su usuario por favor ingrese al siguiente enlace: <br>"+
                enlace+usuId +
                "</p>"+
                "<p style=\"color:#b3b3b3;font-size:12px;text-align:center;margin:30px;font-family:roboto 0 0\">ClinicaUNA</p>"+
                "<p></p></div>"+
                "</td>"+
                "</tr>"+
                "</tbody></table>"
                );
            }); 
             thread.start();
             }
    }catch(Exception ex)
    {

    }
}
    
    @FXML
    private void evtLimpiar(ActionEvent event) 
    {
        Limpiar();
    }
    
     @FXML
    private void evtEliminar(ActionEvent event) 
    {
        usuarioService = new UsuarioService();
        Respuesta res=null;
        if(usuario.getUsuId() != null)
        {
            res= usuarioService.eliminarUsuario(usuario.getUsuId());
            if(!res.getEstado())
            {
                new Mensaje().showModal(Alert.AlertType.WARNING, "", getStage(),FlowController.getInstance().getLanguage().getString("msjEGUsu"));
                System.out.println(res.getMensaje());
            }
            else
            {
                Limpiar();
                new Mensaje().showModal(Alert.AlertType.WARNING, "",getStage(), FlowController.getInstance().getLanguage().getString("msjEUsu"));
            }
        }
    }
    void Limpiar()
    {
        tfContra.clear();
        tfUsuario.clear();
        tfNombre.clear();
        tfSApellido.clear();
        tfPApellido.clear();
        tfCorreo.clear();
        tfCedula.clear();
        cmbTipo.getSelectionModel().clearSelection();
        cmbIdioma.getSelectionModel().clearSelection();
        usuario = new UsuarioDto();
        //usuarioActual = new UsuarioDto();
        modificado = false;
        unbindUsuario();
    }

    @FXML
    private void evtBuscar(ActionEvent event) 
    {
        if(usuarioActual.getUsuTipo().equals("A")){
        FlowController.getInstance().goViewInWindowModal("buscarUsuarios", getStage(), false);
        buscar = true;
        cargarUsuario();
        }else{
            Limpiar();
            Respuesta respuesta = us.getUsuId(ua);
            usuarioActual = (UsuarioDto) respuesta.getResultado("Usuario");
            usuario = usuarioActual;//(UsuarioDto) AppContext.getInstance().get("usuarioB");
            bindUsuario();
            switch (usuario.getUsuTipo()) 
            {
                case "A":
                    cmbTipo.getSelectionModel().select(0);
                    break;
                case "M":
                    cmbTipo.getSelectionModel().select(2);
                    break;
                case "R":
                    cmbTipo.getSelectionModel().select(1);
                    break;
                default:
                    break;
            }
        //}
            if(usuario.getUsuIdioma().equals("I"))
            {
                cmbIdioma.getSelectionModel().select(1);
            }
            else
            {
                cmbIdioma.getSelectionModel().select(0);
            }
        }
    }
    

    public void cargar(UsuarioDto usu)
    {
        tfNombre.setText(usu.getUsuNombre());
        tfCedula.setText(usu.getUsuCedula());
        tfPApellido.setText(usu.getUsuPapellido());
        tfSApellido.setText(usu.getUsuSapellido());
        tfUsuario.setText(usu.getUsuUsuario());
        tfContra.setText(usu.getUsuContrasena());
        tfCorreo.setText(usu.getUsuCorreo());
        if(usu.getUsuTipo() != null)
        {
            switch (cmbTipo.getSelectionModel().getSelectedIndex()) 
            {
                case 0:
                    usu.setUsuTipo("A");
                    break;
                case 2:
                    usu.setUsuTipo("M");
                    break;
                case 1:
                    usu.setUsuTipo("R");
                    break;
                default:
                    break;
            }
        }
        if(cmbIdioma.getSelectionModel().getSelectedIndex()==0)
        {
            usu.setUsuIdioma("E");
        }
        else
        {
            usu.setUsuIdioma("I");
        }
    }
    
}
