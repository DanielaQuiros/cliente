/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.ArchivoDto;
import clinicauna.model.ExpedienteDto;
import clinicauna.model.PacienteDto;
import clinicauna.service.ArchivoService;
import clinicauna.service.ExpedienteService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class ArchivosController extends Controller implements Initializable {

    @FXML
    private TableColumn<ArchivoDto, String> tcTipo;
    @FXML
    private TableColumn<ArchivoDto, String> tcFecha;
    @FXML
    private TableColumn<ArchivoDto, String> tcNombreExamen;
    @FXML
    private TableColumn<ArchivoDto, String> tcAnotaciones;
    @FXML
    private TableView<ArchivoDto> tblArchivos;
    @FXML
    private Label lblPaciente;
    
    private PacienteDto paciente;
    private ExpedienteDto expediente;
    private ExpedienteService expS;
    private ObservableList<ArchivoDto> archivos;
    private ArchivoService archS;
            

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcAnotaciones.setCellValueFactory(x-> x.getValue().anotaciones);
        tcFecha.setCellValueFactory(x-> new SimpleStringProperty(x.getValue().getFecha().toString()));
        tcNombreExamen.setCellValueFactory(x-> x.getValue().nombreExamen);
        tcTipo.setCellValueFactory(x -> x.getValue().tipo);
        
        expS = new ExpedienteService();
        archS = new ArchivoService();
        archivos = FXCollections.observableArrayList();
    }    

    @Override
    public void initialize() {
        paciente = new PacienteDto();
        expediente = new ExpedienteDto();
        
        paciente = (PacienteDto) AppContext.getInstance().get("buscarPaciente");
        Respuesta res = expS.getExpediente(paciente.getPacId());
        expediente = (ExpedienteDto) res.getResultado("expediente");
        
        lblPaciente.setText(FlowController.getInstance().getLanguage().getString("paciente") + ": "+ paciente.getPacNombre() + " " +paciente.getPacApellidos());
        
        res = archS.getArchivo(expediente.getId());
        if(res.getEstado()){
            if(archivos != null){
                archivos = (FXCollections.observableArrayList((List<ArchivoDto>)res.getResultado("archivos")));
                tblArchivos.setItems(archivos);
            }
        }
        //AppContext.getInstance().set("archivo", archivos.get(0));
    }

    @FXML
    private void evtAgregarArchivo(ActionEvent event) {
       AppContext.getInstance().set("expedienteActual", expediente);
       AppContext.getInstance().set("archivoActual", null);
       FlowController.getInstance().goView("AgregarArchivo"); 
    }

    @FXML
    private void evtVistaPrevia(ActionEvent event) {
        if(AppContext.getInstance().get("archivoActual") != null)
            FlowController.getInstance().goView("AgregarArchivo");
        else 
            new Mensaje().show(Alert.AlertType.WARNING, "", FlowController.getInstance().getLanguage().getString("mjsSelecArchivo"));
    }

    @FXML
    private void evtAtras(ActionEvent event) {
        FlowController.getInstance().goView("MantExpedientes");
    }

    @FXML
    private void evtTblArchivos(MouseEvent event) {
        if(tblArchivos.getSelectionModel().getSelectedItem() != null){
            AppContext.getInstance().set("archivoActual", tblArchivos.getSelectionModel().getSelectedItem());
        }
    }
    
    
}
