/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.EspecialidadDto;
import clinicauna.model.MedicoDto;
import clinicauna.model.UsuarioDto;
import clinicauna.service.EspecialidadService;
import clinicauna.service.MedicoService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class MantMedicosController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfCodigo;
    @FXML
    private JFXTextField tfFolio;
    @FXML
    private JFXTextField tfCarne;
    @FXML
    private JFXTimePicker tpHoraInicio;
    @FXML
    private JFXTextField tfCantE;
    @FXML
    private JFXTimePicker tpHoraFin;
    @FXML
    private JFXTextField tfNombre;
    @FXML
    private JFXTextField tfApellido;
    @FXML
    private JFXTextField tfCedula;
    @FXML
    private JFXTextField tfCorreo;
    @FXML
    private JFXTextField tfEsp;
    
    MedicoService medicoService;
    EspecialidadService espService;
    MedicoDto med;
    EspecialidadDto esp;
    UsuarioDto usuario;
    boolean buscar,buscarM,buscarE;
    List<Node> requeridos = new ArrayList<>();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        
    }
    @Override
    public void initialize() 
    {
        med = new MedicoDto();
        esp = new EspecialidadDto();
        medicoService = new MedicoService();
        espService = new EspecialidadService();
        usuario = new UsuarioDto();
        buscar = false;
        buscarM = false;
        buscarE = false;
    }
    public void agregarRequeridos() 
    {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(tfCarne, tfFolio, tfCantE, tfCodigo,tfEsp,tpHoraFin,tpHoraFin));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return FlowController.getInstance().getLanguage().getString("camposRequeridos") + " [" + invalidos + "].";
        }
    }

    private void bindMedico() 
    {
        tfCarne.textProperty().bindBidirectional(med.medCarne);
        tfCantE.textProperty().bindBidirectional(med.medEspaciosPorHora);
        tfFolio.textProperty().bindBidirectional(med.medFolio);
        tfCodigo.textProperty().bindBidirectional(med.medCodigo);
        tpHoraFin.valueProperty().bindBidirectional(med.medFinjornada);
        tpHoraInicio.valueProperty().bindBidirectional(med.medIniciojornada);
    }
    private void bindUsuario()
    {
        tfNombre.textProperty().bindBidirectional(usuario.usuNombre);
        tfApellido.textProperty().bindBidirectional(usuario.usuPapellido);
        tfCedula.textProperty().bindBidirectional(usuario.usuCedula);
        tfCorreo.textProperty().bindBidirectional(usuario.usuCorreo);
    }
    private void unbindMedico()
    {
        tfCarne.textProperty().unbindBidirectional(med.medCarne);
        tfCantE.textProperty().unbindBidirectional(med.medEspaciosPorHora);
        tfFolio.textProperty().unbindBidirectional(med.medFolio);
        tfCodigo.textProperty().unbindBidirectional(med.medCodigo);
        tpHoraFin.valueProperty().unbindBidirectional(med.medFinjornada);
        tpHoraInicio.valueProperty().unbindBidirectional(med.medIniciojornada); 
    }  
    private void nuevoMedico()
    {
        unbindMedico();
        usuario= new UsuarioDto();
        bindMedico();
        
    }
    public void cargarUsuario()
    {
        if(buscar && AppContext.getInstance().get("usuarioB") != null)
        {
            usuario = (UsuarioDto) AppContext.getInstance().get("usuarioB");
            bindUsuario();
        }
        buscar = false;
    }
    public void cargarMedico()
    {
        if(buscarM && AppContext.getInstance().get("medico") != null)
        {
            med = (MedicoDto)AppContext.getInstance().get("medico");
            bindMedico();
        }
        buscarM = false;
    }
    public void cargarEsp()
    {
        if(buscarE && AppContext.getInstance().get("EspB")!= null)
        {
            esp = (EspecialidadDto)AppContext.getInstance().get("EspB");
            tfEsp.textProperty().bindBidirectional(esp.espNombre);
        }
        buscarE = false;
    }
    @FXML
    private void evtBuscar(ActionEvent event) 
    {
        FlowController.getInstance().goViewInWindowModal("buscarMedicos", getStage(), Boolean.FALSE);
        buscarM = true;
        cargarMedico();
        med = (MedicoDto) AppContext.getInstance().get("medico");
        tfNombre.setText(med.getMedUsuId().getUsuNombre());
        tfApellido.setText(med.getMedUsuId().getUsuPapellido());
        tfCedula.setText(med.getMedUsuId().getUsuCedula());
        tfCorreo.setText(med.getMedUsuId().getUsuCorreo());
        tfEsp.setText(med.getMedEspId().getEspNombre());
    }

    @FXML
    private void evtGuardar(ActionEvent event) 
    {
       if (!tfCarne.getText().isEmpty() && !tfCodigo.getText().isEmpty() && !tfCantE.getText().isEmpty() && !tfFolio.getText().isEmpty()) {
            med.setMedCarne(tfCarne.getText());
            med.setMedCodigo(tfCodigo.getText());
            med.setMedEspaciosPorHora(Integer.valueOf(tfCantE.getText()));
            med.setMedFolio(tfFolio.getText());
            med.setMedIniciojornada(tpHoraInicio.getValue());
            med.setMedFinjornada(tpHoraFin.getValue());
            usuario = (UsuarioDto) AppContext.getInstance().get("usuarioB");
            med.setMedUsuId(usuario);
            esp = (EspecialidadDto)AppContext.getInstance().get("EspB"); 
            med.setMedEspId(esp);
           // bindMedico();
            Respuesta respuesta = medicoService.guardarMedico(med);
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "", getStage(), FlowController.getInstance().getLanguage().getString("msjGMed"));

        } 
        else 
        {
            new Mensaje().showModal(Alert.AlertType.ERROR, "", getStage(), FlowController.getInstance().getLanguage().getString("msjEGMed"));
        } 
    }

    @FXML
    private void evtEliminar(ActionEvent event) 
    {
        med = (MedicoDto) AppContext.getInstance().get("medico");
        if (med != null) 
        {
            medicoService.eliminarMedico(med.getMedId());
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "", getStage(), FlowController.getInstance().getLanguage().getString("msjEMed"));
        } 
        else 
        {
            new Mensaje().showModal(Alert.AlertType.ERROR, "", getStage(), FlowController.getInstance().getLanguage().getString("msjEGMed"));
        }
    }

    @FXML
    private void evtLimpiar(ActionEvent event) 
    {
        Limpiar();
    }


    @FXML
    private void evtUsuario(ActionEvent event) 
    {
        FlowController.getInstance().goViewInWindowModal("buscarUsuarios", getStage(), false);
        buscar = true;
        cargarUsuario();
   
    }

    
    public void Limpiar() 
    {
        tfCantE.clear();
        tfCarne.clear();
        tfFolio.clear();
        tfNombre.clear();
        tfCodigo.clear();
        tfApellido.clear();
        tfCedula.clear();
        tfCorreo.clear();
        tfEsp.clear();

    }

    @FXML
    private void evtEsp(MouseEvent event) 
    {
        FlowController.getInstance().goViewInWindowModal("buscarEspecialidad",getStage(),false);  
        buscarE = true;
        cargarEsp();
        
    }
    
}
