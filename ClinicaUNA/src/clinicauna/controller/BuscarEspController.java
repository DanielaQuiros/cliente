/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.EspecialidadDto;
import clinicauna.service.EspecialidadService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class BuscarEspController extends Controller implements Initializable {

    @FXML
    private JFXTextField tf_NomEsp;
    @FXML
    private TableView<EspecialidadDto> tbEspecialidad;
    @FXML
    private TableColumn<EspecialidadDto,String> tcNombre;
    
    ObservableList<EspecialidadDto> esp;
    EspecialidadService espService;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        tcNombre.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getEspNombre()));
    }    

    @Override
    public void initialize() 
    {
        espService = new EspecialidadService();
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        if(!tf_NomEsp.getText().isEmpty())
        {
            tbEspecialidad.getItems().clear();
            Respuesta res=espService.getEspecialidad(tf_NomEsp.getText().isEmpty() ? "%" :tf_NomEsp.getText());
            List< EspecialidadDto> e = (List<EspecialidadDto>)res.getResultado("Especialidad");
            
            if(res.getEstado()){
                esp= FXCollections.observableArrayList(e);
                tbEspecialidad.setItems(esp);
            }
        }
        else
        {
            new Mensaje().showModal(Alert.AlertType.ERROR, "", getStage(),FlowController.getInstance().getLanguage().getString("msjEE"));

        }
        
    }

    @FXML
    private void evtAceptar(ActionEvent event) {
        if(tbEspecialidad.getSelectionModel().getSelectedItem() != null){
            EspecialidadDto espDto = tbEspecialidad.getSelectionModel().getSelectedItem();
            AppContext.getInstance().set("EspBusq",espDto);
            getStage().close();
        }
        
    }
    
}
