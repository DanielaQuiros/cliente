/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.PacienteDto;
import clinicauna.model.UsuarioDto;
import clinicauna.service.PacienteService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class BuscarPacientesController extends Controller implements Initializable {

    @FXML
    private TableView<PacienteDto> tblPacientes;
    @FXML
    private TableColumn<PacienteDto, String> tcNombre;
    @FXML
    private TableColumn<PacienteDto, String> tcApellido;
    @FXML
    private TableColumn<PacienteDto, String> tcCedula;
    @FXML
    private JFXTextField tfNombre;
    @FXML
    private JFXTextField tfApellido;
    @FXML
    private JFXTextField tfCedula;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnAceptar;

    ObservableList<PacienteDto> pacientes;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNombre.setCellValueFactory(x -> x.getValue().pacNombre);
        tcApellido.setCellValueFactory(x -> x.getValue().pacApellidos);
        tcCedula.setCellValueFactory(x -> new SimpleStringProperty(x.getValue().getPacCedula()));
        
        pacientes = FXCollections.observableArrayList();
    }    

    @Override
    public void initialize() {
        limpiar();
        
    }

    @FXML
    private void evtTblPacientes(MouseEvent event) {
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        PacienteService ps = new PacienteService();
        
        Respuesta respuesta = new Respuesta();
        
        String ced = "%";
        String nom = "%";
        String ap = "%";
        
        if(!tfCedula.getText().isEmpty()){
            ced = tfCedula.getText();
        }
        if(!tfNombre.getText().isEmpty()){
            nom = tfNombre.getText();
        }
        if(!tfApellido.getText().isEmpty()){
            ap = tfApellido.getText();
        }
        
        respuesta = ps.getPaciente(ced, nom, ap);
        
        if(respuesta.getEstado()){
            respuesta.getResultado("pacientes");
            pacientes = (FXCollections.observableArrayList((List<PacienteDto>)respuesta.getResultado("pacientes")));
            if(pacientes.isEmpty())
                new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("sinResultados"));
            tblPacientes.setItems(pacientes);
        }else{
            new Mensaje().show(Alert.AlertType.WARNING, " ", FlowController.getInstance().getLanguage().getString("errorBuscar")+ respuesta.getMensajeInterno());
        }
    }
    
    public void limpiar(){
        tfCedula.clear();
        tfNombre.clear();
        tfApellido.clear();
        
        tblPacientes.getItems().clear();
    }

    @FXML
    private void evtAceptar(ActionEvent event) {
        if(tblPacientes.getSelectionModel().getSelectedItem() != null){
            AppContext.getInstance().set("buscarPaciente", tblPacientes.getSelectionModel().getSelectedItem());
            limpiar();
            getStage().close();
        }else
            if(new Mensaje().showConfirmation("", getStage(), FlowController.getInstance().getLanguage().getString("sinSeleccion"))){
                limpiar();
                getStage().close();
            }
    }
}
