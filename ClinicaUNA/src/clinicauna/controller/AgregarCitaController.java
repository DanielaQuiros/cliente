/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.CitaDto;
import clinicauna.model.MedicoDto;
import clinicauna.model.PacienteDto;
import clinicauna.model.UsuarioDto;
import clinicauna.service.CitaService;
import clinicauna.service.MedicoService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class AgregarCitaController extends Controller implements Initializable {

    @FXML
    private JFXDatePicker dpFecha;
    @FXML
    private JFXComboBox<LocalTime> cbHoras;
    @FXML
    private JFXTextArea txaMotivo;
    @FXML
    private JFXButton bntGuardar;
    @FXML
    private JFXButton bntBuscar;
    
    MedicoDto medico;
    PacienteDto paciente;
    UsuarioDto usuario;
    CitaDto cita;
    List<LocalTime> listHras;
    boolean buscar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @Override
    public void initialize() {
        dpFecha.setValue(null);
        txaMotivo.clear();
        cbHoras.getItems().clear();
        
        buscar = false;
        cita = new CitaDto();
        paciente = new PacienteDto();
        paciente = (PacienteDto) AppContext.getInstance().get("buscarPaciente");
        
        Respuesta respuesta = new MedicoService().getMedico(((UsuarioDto)AppContext.getInstance().get("usuarioActual")).getUsuId());
        medico = (MedicoDto) respuesta.getResultado("Medico");
        
        generar();
    }

    @FXML
    private void evtFechas(ActionEvent event) {
    }

    @FXML
    private void evtGuardar(ActionEvent event) {
        if(!buscar){
            if(!txaMotivo.getText().isEmpty() && dpFecha.getValue() != null && cbHoras.getValue() != null){
                cita.setCitMotivo(txaMotivo.getText());
                cita.setCitHora(cbHoras.getValue().toString());
                cita.setCitFecha(dpFecha.getValue());
                cita.setCitMedId(medico);
                cita.setPaciente(paciente);
                cita.setCitEstado("P");
                cita.setCitTelefono(paciente.getPacTelefono());
                cita.setCitCorreo(paciente.getPacCorreo());
                cita.setCitEspacios(1);
                
                Respuesta resp = new CitaService().guardarcita(cita);
                    if (!resp.getEstado()) {
                        //new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Cita", getStage(), "Cita Guardado con exito");
                        cita = new CitaDto();
                        cita = (CitaDto) resp.getResultado("Cita");
                        AppContext.getInstance().set("citaBusqueda", cita);
                        getStage().close();
                        FlowController.getInstance().goView("MantConsulta");
                    }else{
                        new Mensaje().show(Alert.AlertType.ERROR, "", FlowController.getInstance().getLanguage().getString("mjsCita"));
                    }
            }else{
                AppContext.getInstance().set("citaBusqueda", cita);
                new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("camposRequeridos"));
            }
        }
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarCitas", getStage(), Boolean.FALSE);
        if(((CitaDto) AppContext.getInstance().get("citaBusqueda"))!=null){
            buscar = true;
            cita = (CitaDto) AppContext.getInstance().get("citaBusqueda");
            txaMotivo.setText(cita.getCitMotivo());
            dpFecha.setValue(cita.getCitFecha());
            cbHoras.setValue(LocalTime.parse(cita.getCitHora()));
            
            AppContext.getInstance().set("citaBusqueda", cita);
            getStage().close();
            FlowController.getInstance().goView("MantConsulta");
        }
    }
    
        public void generar() {
        if(medico.getMedIniciojornada() != null && medico.getMedFinjornada() != null){
            listHras = new ArrayList();
            Integer suma = (60 / medico.getMedEspaciosPorHora());
            //Integer suma = (60 / 4);
            LocalTime hora = medico.getMedIniciojornada().minusMinutes(suma);
            LocalTime fin = medico.getMedFinjornada();
            while (hora.isBefore(fin.minusMinutes(suma))) {
                hora = hora.plusMinutes(suma);
                listHras.add(hora);

            }
            remover(listHras);
            cbHoras.setItems(FXCollections.observableList(listHras));
        }else{
            new Mensaje().show(Alert.AlertType.WARNING, "", FlowController.getInstance().getLanguage().getString("mjsMedHorario"));
        }
    }
    
    public void remover(List<LocalTime> list) {
        try {
            Respuesta resp = new CitaService().allcitas(medico.getMedId(), dpFecha.getValue().toString());
            if (resp.getEstado()) {
                List<CitaDto> citas = (List<CitaDto>) resp.getResultado("citas");
                for (CitaDto cit : citas) {
                    list.remove(LocalTime.parse(cit.getCitHora()));
                }
                list.add(0, LocalTime.parse(cita.getCitHora()));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
}
