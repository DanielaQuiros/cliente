/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.service.UsuarioService;
import clinicauna.util.ContrasenaRandom;
import clinicauna.util.Correo;
import clinicauna.util.FlowController;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class RecContrasenaController extends Controller implements Initializable 
{
   
    @FXML
    private JFXTextField tfCorreoR;
    Correo correo;
    UsuarioService usuario;
    ContrasenaRandom contraR;
    String contra;
    
    @Override 
    public void initialize(URL url, ResourceBundle rb) 
    {
        
    }    

    @FXML
    private void evtEnviar(ActionEvent event) 
    {
        contra=contraR.generarContrasena();
        if(!tfCorreoR.getText().isEmpty())
        {
            usuario.recuperarUsuario(tfCorreoR.getText(), contra);
            Thread thread = new Thread(()->{
               correo.EnviarTexto(tfCorreoR.getText(),"Contraseña Temporal",
                       "<table style=\"max-width:600px;padding:10px;margin:0 auto;border-collapse:collapse\">"+
                        "<tbody><tr>"+
                        "<td style=\"background-color:#E1F5FE\">"+
                        "<div style=\"color:#34495e;margin:4% 10% 2%;text-align:justify;font-family:candara\">"+
                        "<h2 style=\"color:#03366;margin:0 0 7px\">Contraseña temporal</h2>"+
                        "<p style=\"margin:2px;font-size:15px;font-family:roboto\">"+
                        "Esta es su contraseña temporal, debe iniciar sesion para cambiar su contraseña <br>"+
                        contra+
                        "</p>"+
                        "<p style=\"color:#b3b3b3;font-size:12px;text-align:center;margin:30px;font-family:roboto 0 0\">ClinicaUNA</p>"+
                        "<p></p></div>"+
                        "</td>"+
                        "</tr>"+
                        "</tbody></table>"
                        );
            }
            );
            thread.start();
        }
    }

    @FXML
    private void evtAtras(ActionEvent event) 
    {
        ((Stage) tfCorreoR.getScene().getWindow()).close();
        FlowController.getInstance().goViewInWindow("LogIn");
    }
    @Override
    public void initialize() 
    {
        correo = new Correo("unaprogra32018@gmail.com","una32018");
        usuario = new UsuarioService();
        contraR = new ContrasenaRandom();
        contra = "";
    }
    
}
