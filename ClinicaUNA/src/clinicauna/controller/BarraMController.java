/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.UsuarioDto;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class BarraMController extends Controller implements Initializable {

    @FXML
    private JFXButton btnPacientes;
    @FXML
    private JFXButton btnCitas;
    @FXML
    private JFXButton btnExpedientes;
    @FXML
    private JFXButton btnUsuarios;
    @FXML
    private JFXButton btnInicio;
    @FXML
    private JFXButton btnLogOut;
    @FXML
    private JFXButton btnDoctores;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
    }

    @FXML
    private void evtPacientes(ActionEvent event) {
        FlowController.getInstance().goView("RegPacientes");
    }

    @FXML
    private void evtDoctores(ActionEvent event) {
        FlowController.getInstance().goView("MantMedicos");
    }

    @FXML
    private void evtUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("MantUsuarios");
    }

    @FXML
    private void evtCitas(ActionEvent event) {
        AppContext.getInstance().set("vengo", "menu");
        FlowController.getInstance().goView("Agenda");
    }

    @FXML
    private void evtExpedientes(ActionEvent event) {
        FlowController.getInstance().goView("MantExpedientes");
       MantExpedientesController ex = (MantExpedientesController) AppContext.getInstance().get("expController");
        ex.limpiar();
    }

    @FXML
    private void evtInicio(ActionEvent event) {
        FlowController.getInstance().goView("Menu");
    }
    @FXML
    private void evtEspecialidad(ActionEvent event) 
    {
       FlowController.getInstance().goView("MantEspecialidad"); 
    }

    @FXML
    private void evtReportes(ActionEvent event) 
    {
        FlowController.getInstance().goView("Reporte");
    }
    @FXML
    private void evtLogOut(ActionEvent event) {
        ((Stage) btnCitas.getScene().getWindow()).close();
        FlowController.getInstance().goViewInWindow("LogIn");
        FlowController.getInstance().clear();

    }

    

}
