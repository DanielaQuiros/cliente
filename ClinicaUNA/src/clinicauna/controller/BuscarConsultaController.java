/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.CitaDto;
import clinicauna.model.ConsultaDto;
import clinicauna.service.CitaService;
import clinicauna.service.ConsultaService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class BuscarConsultaController extends Controller implements Initializable {

    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private JFXTextField tfCita;
    @FXML
    private Label lblAn;
    @FXML
    private Label lblTrat;
    @FXML
    private Label lblExam;
    @FXML
    private Label lblMotivo;
    
    ObservableList<ConsultaDto> consultas;
    ConsultaService conService;
    CitaService citaService;
    ConsultaDto conDto;
    CitaDto citaDto;
    boolean buscar;
   
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        buscar = false;
        conService = new ConsultaService();
        citaService = new CitaService();
        conDto = new ConsultaDto();
        citaDto = new CitaDto();
    }    
    @Override
    public void initialize() 
    {
        tfCita.clear();
    }
    @FXML
    private void evtBuscar(ActionEvent event) 
    {
        if(!tfCita.getText().isEmpty())
        {
            Respuesta r = conService.getConsulta(citaDto.getCitId());
            conDto= (ConsultaDto)r.getResultado("Consultas");
            lblAn.setText(conDto.getConAnotaciones());
            lblExam.setText(conDto.getConExamenes());
            lblMotivo.setText(citaDto.getCitMotivo());
            lblTrat.setText(conDto.getConTratamiento());
        }
        else
        {
            new Mensaje().showModal(Alert.AlertType.ERROR, "", getStage(),FlowController.getInstance().getLanguage().getString("msjConCita"));
        }
    }

    @FXML
    private void evtAceptar(ActionEvent event) 
    {
        AppContext.getInstance().set("consultaB", conDto);
        getStage().close();
    }
    public void cargarCita()
    {
        if(buscar && AppContext.getInstance().get("citaBusqueda") != null)
        {
            citaDto = (CitaDto)AppContext.getInstance().get("citaBusqueda");
            tfCita.setText(citaDto.getCitFecha().toString());
        }
    }
    @FXML
    private void evt_Cita(MouseEvent event) 
    {
        FlowController.getInstance().goViewInWindowModal("buscarCitas",getStage(),false);
        buscar = true;
        cargarCita();
    }

    
    
}
