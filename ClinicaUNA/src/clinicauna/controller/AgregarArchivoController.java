/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.ArchivoDto;
import clinicauna.model.ExpedienteDto;
import clinicauna.service.ArchivoService;
import clinicauna.util.AppContext;
import clinicauna.util.ConverArchivo;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import org.apache.commons.io.FilenameUtils;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class AgregarArchivoController extends Controller implements Initializable {

    @FXML
    private JFXComboBox<String> cbTipoArchivo;
    @FXML
    private JFXDatePicker dpFecha;
    @FXML
    private JFXTextField tfNombreExamen;
    @FXML
    private JFXTextArea taAnotaciones;
    @FXML
    private JFXButton bntGuardar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private JFXButton btnVistaPrevia;
    
    ExpedienteDto expediente;
    ArchivoService archS;
    ArchivoDto archivo;
    FileChooser fileChooser;
    File file;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        archS = new ArchivoService();
        fileChooser = new FileChooser();
        fileChooser.setTitle(FlowController.getInstance().getLanguage().getString("archivos"));
    }

    @FXML
    private void evtCargar(ActionEvent event) throws URISyntaxException, IOException {
        if (cbTipoArchivo.getSelectionModel().getSelectedItem() != null) {
            file = fileChooser.showOpenDialog(getStage());
            if (file != null) {
                if(cbTipoArchivo.getSelectionModel().getSelectedItem().equals("PDF")){
                    String resultado = ConverArchivo.fileToString(file.toURI().toString());
                    archivo.setArchivo(resultado);
                }else{
                    String tipo = FilenameUtils.getExtension(file.getPath());
                    archivo.setTipo(tipo);
                    BufferedImage im = ImageIO.read(file);
                    String resultado = ConverArchivo.imageToString(im, tipo);
                    archivo.setArchivo(resultado);
                }
            }
        }else
            new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("mjsTipoArch"));
    }

    @FXML
    private void evtTipoArchivo(ActionEvent event) {
        if (cbTipoArchivo.getSelectionModel().getSelectedItem() != null) {
            if (cbTipoArchivo.getSelectionModel().getSelectedItem().equals("PDF")) {
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF Files", "*.pdf"));
                archivo.setTipo("PDF");
            } else {
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image files", "*.jpg", "*.JPG", "*.png", "*.PNG"));
                archivo.setTipo("IMG");
            }
        }
    }

    @FXML
    private void evtGuardar(ActionEvent event) {
        if (tfNombreExamen.getText().isEmpty() || dpFecha.getValue() == null || taAnotaciones.getText().isEmpty()) {
            new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("camposRequeridos"));
        } else {
            if(file != null){
                archivo.setAnotaciones(taAnotaciones.getText());
                archivo.setExpediente(expediente);
                archivo.setFecha(dpFecha.getValue());
                archivo.setNombreExamen(tfNombreExamen.getText());
                Respuesta res = archS.guardarArchivo(archivo);
                if(!res.getEstado()){
                    new Mensaje().show(Alert.AlertType.ERROR,"",FlowController.getInstance().getLanguage().getString("errorGuardarArchivo") + res.getMensaje() + res.getMensajeInterno());
                }else{
                    archivo = (ArchivoDto) res.getResultado("Archivo");
                    new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("archGuardado"));
                }
            }else{
                new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("mjSelecArch"));
            }
        }
    }

    @FXML
    private void btnAtras(ActionEvent event) {
        FlowController.getInstance().goView("Archivos");
    }

    @Override
    public void initialize() {
        limpiar();

        cbTipoArchivo.getItems().clear();
        cbTipoArchivo.getItems().addAll(FlowController.getInstance().getLanguage().getString("imagen"), "PDF");

        expediente = new ExpedienteDto();
        archivo = new ArchivoDto();
        expediente = (ExpedienteDto) AppContext.getInstance().get("expedienteActual");
        
        if(AppContext.getInstance().get("archivoActual") != null){
            archivo = (ArchivoDto) AppContext.getInstance().get("archivoActual");
            bind();
            AppContext.getInstance().set("archivoActual", null);
        }
    }

    public void limpiar() {
        taAnotaciones.clear();
        tfNombreExamen.clear();
        dpFecha.setValue(null);
        cbTipoArchivo.getSelectionModel().clearSelection();
    }

    @FXML
    private void evtEliminar(ActionEvent event) {
        Respuesta res = archS.eliminarArchivo(archivo.id);
        if(res.getEstado()){
            limpiar();
        }else{
            System.out.println(res.getMensaje() + " " + res.getMensajeInterno());
        }
    }

    @FXML
    private void evtLimpiar(ActionEvent event) {
        limpiar();
    }

    public void bind(){
        taAnotaciones.textProperty().bindBidirectional(archivo.anotaciones);
        tfNombreExamen.textProperty().bindBidirectional(archivo.nombreExamen);
        dpFecha.valueProperty().bindBidirectional(archivo.fecha);
    }
    
    public void unBind(){
        taAnotaciones.textProperty().unbindBidirectional(archivo.anotaciones);
        tfNombreExamen.textProperty().unbindBidirectional(archivo.nombreExamen);
        dpFecha.valueProperty().unbindBidirectional(archivo.fecha);
    }

    @FXML
    private void evtVistaPrevia(ActionEvent event) throws IOException {
        Respuesta res = archS.archivo(archivo.getId());
        String path = "";
        if(res.getEstado()){
            archivo = (ArchivoDto) res.getResultado("Archivo");
            if(archivo.getTipo().equals("PDF"))
                path = ConverArchivo.stringToFile(archivo.getArchivo(), archivo);
            else 
                path = ConverArchivo.guardarImagen(archivo.getArchivo(), archivo);
        }else
            System.out.println("error");
        
        if (Desktop.isDesktopSupported()){
            File file = new File(path);
            Desktop.getDesktop().open(file);
        }
    }
}
