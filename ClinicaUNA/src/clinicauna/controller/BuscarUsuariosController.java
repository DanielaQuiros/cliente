/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.UsuarioDto;
import clinicauna.service.UsuarioService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class BuscarUsuariosController extends Controller implements Initializable {

    @FXML
    private TableView<UsuarioDto> tblUsuarios;
    @FXML
    private TableColumn<UsuarioDto,String> tcNombre;
    @FXML
    private TableColumn<UsuarioDto, String> tcApellido;
    @FXML
    private TableColumn<UsuarioDto, String> tcCedula;
    @FXML
    private JFXTextField tfNombre;
    @FXML
    private JFXTextField tfApellido;
    @FXML   
    private JFXTextField tfCedula;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnAceptar;
    
    UsuarioService usuario;
    ObservableList<UsuarioDto> usuarios;
    Respuesta respuesta;

    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        tcNombre.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getUsuNombre()));
        tcApellido.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getUsuPapellido()));
        tcCedula.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getUsuCedula()));
    }    
    @FXML
    private void evtBuscar(ActionEvent event) 
    {
        tblUsuarios.getItems().clear();
        tfApellido.clear();
        tfCedula.clear();
        tfNombre.clear();
        Respuesta res=usuario.getUsuarios(tfNombre.getText().isEmpty()? " " : tfNombre.getText(),tfCedula.getText().isEmpty()? " " : tfCedula.getText(), tfApellido.getText().isEmpty() ? " " :tfApellido.getText());
        if(res.getEstado())
        {
            usuarios= FXCollections.observableArrayList((List<UsuarioDto>)res.getResultado("Usuarios"));

            tblUsuarios.setItems(usuarios);
        }
        else
        {
            System.out.println(res.getMensaje());
        }

    }

    @FXML
    private void evtAceptar(ActionEvent event) 
    {
        if(tblUsuarios.getSelectionModel().getSelectedItem() != null)
        {
            UsuarioDto usu= tblUsuarios.getSelectionModel().getSelectedItem();
            AppContext.getInstance().set("usuarioB",usu);
            getStage().close();
            Limpiar();
        }
    }


    @Override
    public void initialize() 
    {
        Limpiar();
        usuario= new UsuarioService();
        respuesta= new Respuesta();
    }
    
    public void Limpiar()
    {
        tfNombre.clear();
        tfApellido.clear();
        tfCedula.clear();
        if(tblUsuarios.getItems() != null)
        {
           tblUsuarios.getItems().clear();
        }
    }
    
}
