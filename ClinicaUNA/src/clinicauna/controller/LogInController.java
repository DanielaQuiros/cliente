/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.UsuarioDto;
import clinicauna.service.UsuarioService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class LogInController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfUsuario;
    @FXML
    private JFXPasswordField tfContrasena;
    
    UsuarioService usuario;
    UsuarioDto usuarioDto;
    boolean contra;


    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        contra =true;
    }    


    @FXML
    private void evtRecuperar(ActionEvent event) 
    {
        ((Stage) tfUsuario.getScene().getWindow()).close();
        FlowController.getInstance().goViewInWindow("RecContrasena");
        contra = false;
        
        FlowController.getInstance().clear();
    }
    
    @FXML
    private void evtIngresar(ActionEvent event) 
    {
        if(!tfUsuario.getText().isEmpty() && !tfContrasena.getText().isEmpty())
        {
            Respuesta respuesta=usuario.getUsuario(tfUsuario.getText(),tfContrasena.getText());
            usuarioDto=(UsuarioDto)respuesta.getResultado("Usuario");
            if(respuesta.getEstado())
            {
                if(respuesta.getEstado().equals(true)  && usuarioDto.getUsuEstado().equals("A"))
                {
                    
                    FlowController.getInstance().goMain();
                    PrincipalController pc = (PrincipalController) AppContext.getInstance().get("Principal");
                    if(usuarioDto.getUsuIdioma().equals("I")){
                        //FlowController.getInstance().setIdioma(ResourceBundle.getBundle("clinicauna/resources/i18n/Ingles"));
                        FlowController.getInstance().loadLanguage(ResourceBundle.getBundle("clinicauna/resources/i18n/Ingles"));
                    }else{
                        FlowController.getInstance().loadLanguage(ResourceBundle.getBundle("clinicauna/resources/i18n/Espanol"));
                    }
                    if(usuarioDto.getUsuTipo().equals("A")){
                        pc.abrirMenu("BarraM");
                        FlowController.getInstance().goView("Menu");
                    }
                    if(usuarioDto.getUsuTipo().equals("R")){
                        pc.abrirMenu("BarraRecep");
                        FlowController.getInstance().goView("MenuRecep");
                    }
                    if(usuarioDto.getUsuTipo().equals("M")){
                        pc.abrirMenu("BarraDoct");
                        FlowController.getInstance().goView("MenuDoct");
                    }
                    ((Stage) tfUsuario.getScene().getWindow()).close();
                    if(!contra)
                    {
                        FlowController.getInstance().goView("MantUsuarios");
                    }
                   // ((Stage) tfUsuario.getScene().getWindow()).close();
                    JFXButton boton =(JFXButton)AppContext.getInstance().get("boton");
                    boton.setDisable(false);
                    AppContext.getInstance().set("usuarioActual", usuarioDto);
                    System.out.println("log in "+usuarioDto.getUsuId());
                }
                else
                {
                   new Mensaje().showModal(Alert.AlertType.ERROR,"", getStage(),FlowController.getInstance().getLanguage().getString("msjEstado"));
                }
            }
            else
            {
                 new Mensaje().showModal(Alert.AlertType.ERROR,"", getStage(),FlowController.getInstance().getLanguage().getString("msjLogIn"));
            }
        }
        else
        {
           new Mensaje().showModal(Alert.AlertType.ERROR,"", getStage(),FlowController.getInstance().getLanguage().getString("msjL"));
        }
    }
    
    @Override
    public void initialize() 
    {
        limpiar();
        usuario= new UsuarioService();
        usuarioDto= new UsuarioDto();
    }
    
    public void limpiar(){
        tfContrasena.clear();
        tfUsuario.clear();
    }
    
    
    
}
