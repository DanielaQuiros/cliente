/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.ExpedienteDto;
import clinicauna.model.PacienteDto;
import clinicauna.service.ExpedienteService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class MantExpedientesController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfNombre;
    @FXML
    private JFXTextField tfApellidos;
    @FXML
    private JFXTextField tfCedula;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXTextArea taAlergias;
    @FXML
    private JFXTextArea taAntPatologicos;
    @FXML
    private JFXTextArea taAntFamiliares;
    @FXML
    private JFXTextArea taHospitalizaciones;
    @FXML
    private JFXTextArea taCirugias;
    @FXML
    private JFXTextArea taTratamiento;
    @FXML
    private JFXButton bntGuardar;
    @FXML
    private JFXButton bntLimpiar;
    @FXML
    private JFXButton bntConsultas;
    @FXML
    private JFXButton btnArchivos;
    
    
    PacienteDto paciente;
    ExpedienteDto expediente;
    ExpedienteService ep;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ep = new ExpedienteService();
        AppContext.getInstance().set("expController", this);
        limpiar();
    }    

    @Override
    public void initialize() {
        
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarPacientes", getStage(), false);
        if(AppContext.getInstance().get("buscarPaciente") != null){
            paciente = (PacienteDto) AppContext.getInstance().get("buscarPaciente");
            cargarPaciente();
            AppContext.getInstance().set("buscarPaciente", null);
            Respuesta res = ep.getExpediente(paciente.getPacId());
            if(res.getEstado()){
                expediente = (ExpedienteDto) res.getResultado("expediente");
                cargarExpediente();
            }
        }
    }

    @FXML
    private void evtGuardar(ActionEvent event) {
        if(paciente.getPacId() != null){
            expediente.setPaciente(paciente);
            Respuesta res = ep.guardarExpediente(expediente);
            if(res.getEstado()){
                expediente = (ExpedienteDto) res.getResultado("expediente");
                new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("mjExpGuardado"));
            }else{
                new Mensaje().show(Alert.AlertType.ERROR, "", FlowController.getInstance().getLanguage().getString("mjExpGuardado")+res.getMensajeInterno());
            }
        }else{
            new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("elegirPaciente"));
        }
        
    }
    
    public void cargarPaciente(){
        tfApellidos.setText(paciente.getPacApellidos());
        tfCedula.setText(paciente.getPacCedula());
        tfNombre.setText(paciente.getPacNombre());
        cargarExpediente();
    }
    
    public void cargarExpediente(){
        taAlergias.textProperty().bindBidirectional(expediente.alergias);
        taAntFamiliares.textProperty().bindBidirectional(expediente.anteFamiliares);
        taAntPatologicos.textProperty().bindBidirectional(expediente.antePalogicos);
        taCirugias.textProperty().bindBidirectional(expediente.operaciones);
        taHospitalizaciones.textProperty().bindBidirectional(expediente.hospitalizaciones);
        taTratamiento.textProperty().bindBidirectional(expediente.tratamientos);
    }
    
    public void unBindExpediente(){
        taAlergias.textProperty().unbindBidirectional(expediente.alergias);
        taAntFamiliares.textProperty().unbindBidirectional(expediente.anteFamiliares);
        taAntPatologicos.textProperty().unbindBidirectional(expediente.antePalogicos);
        taCirugias.textProperty().unbindBidirectional(expediente.operaciones);
        taHospitalizaciones.textProperty().unbindBidirectional(expediente.hospitalizaciones);
        taTratamiento.textProperty().unbindBidirectional(expediente.tratamientos);
    }
    
    public void limpiar(){
        tfApellidos.clear();
        tfCedula.clear();
        tfNombre.clear();
        
        taAlergias.clear(); 
        taAntFamiliares.clear(); 
        taAntPatologicos.clear(); 
        taCirugias.clear(); 
        taHospitalizaciones.clear();
        taTratamiento.clear();
        
        paciente = new PacienteDto();
        expediente = new ExpedienteDto();
        unBindExpediente();
    }

    @FXML
    private void evtLimpiar(ActionEvent event) {
        limpiar();
    }

    @FXML
    private void evtConsultas(ActionEvent event) {
        if(paciente.getPacId() != null){
            AppContext.getInstance().set("buscarPaciente", paciente);
            cargarPaciente();
            FlowController.getInstance().goView("HistorialConsultas");    
        }else{
            FlowController.getInstance().goViewInWindowModal("buscarPacientes", getStage(), Boolean.FALSE);
            if(AppContext.getInstance().get("buscarPaciente") != null){
                FlowController.getInstance().goView("HistorialConsultas");
            }else{
                new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("selecPaciente"));
            }
        }
    }

    @FXML
    private void evtArchivos(ActionEvent event) {
        if(paciente.getPacId() != null){
            AppContext.getInstance().set("buscarPaciente", paciente);
            cargarPaciente();
            FlowController.getInstance().goView("Archivos");   
        }else{
            FlowController.getInstance().goViewInWindowModal("buscarPacientes", getStage(), Boolean.FALSE);
            if(AppContext.getInstance().get("buscarPaciente") != null){
                FlowController.getInstance().goView("Archivos");
            }else{
                new Mensaje().show(Alert.AlertType.INFORMATION, "", FlowController.getInstance().getLanguage().getString("selecPaciente"));
            }
        }
    }
}
