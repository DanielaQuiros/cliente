/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.util.FlowController;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class BarraRecepController extends Controller implements Initializable {

    @FXML
    private JFXButton btnPacientes;
    @FXML
    private JFXButton btnCitas;
    @FXML
    private JFXButton btnUsuarios;
    @FXML
    private JFXButton btnInicio;
    @FXML
    private JFXButton btnLogOut;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void evtPacientes(ActionEvent event) {
        FlowController.getInstance().goView("RegPacientes");
    }

    @FXML
    private void evtCitas(ActionEvent event) {
        FlowController.getInstance().goView("Agenda");
    }

    @FXML
    private void evtUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("MantUsuarios");
    }

    @FXML
    private void evtInicio(ActionEvent event) {
        FlowController.getInstance().goView("MenuRecep");
    }

    @FXML
    private void evtLogOut(ActionEvent event) {
        ((Stage) btnCitas.getScene().getWindow()).close();
        FlowController.getInstance().goViewInWindow("LogIn");
    }

    @Override
    public void initialize() {
        
    }
    
}
