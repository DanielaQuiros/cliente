/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.MedicoDto;
import clinicauna.model.PacienteDto;
import clinicauna.model.UsuarioDto;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class MenuController extends Controller implements Initializable {

    @FXML
    private JFXButton btnPacientes;
    @FXML
    private JFXButton btnUsuarios;
    @FXML
    private JFXButton btnCitas;
    @FXML
    private JFXButton btnExpedientes;
    @FXML
    private JFXButton btnDoctores;
    @FXML
    private JFXButton btnReportes;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       // UsuarioDto b = (UsuarioDto)AppContext.getInstance().get("usuarioActual");
        //System.out.println(b.getUsuNombre());
    }

    @Override
    public void initialize(){

    }

    @FXML
    private void evtPacientes(ActionEvent event) {
        FlowController.getInstance().goView("RegPacientes");
    }

    @FXML
    private void evtDoctores(ActionEvent event) {
        FlowController.getInstance().goView("MantMedicos");
    }

    @FXML
    private void evtUsuarios(ActionEvent event) {
        FlowController.getInstance().goView("MantUsuarios");
    }

    @FXML
    private void evtCitas(ActionEvent event) {
        AppContext.getInstance().set("vengo", "menu");
        FlowController.getInstance().goView("Agenda");
    }

    @FXML
    private void evtExpedientes(ActionEvent event) {
        FlowController.getInstance().goView("MantExpedientes");
        MantExpedientesController ex = (MantExpedientesController) AppContext.getInstance().get("expController");
        ex.limpiar();
    }

    public void pruebas() {
        MedicoDto med = new MedicoDto(null, "1322", "asd", "qw23", "06:00", "07:00", "Pediatria", "3");
        UsuarioDto user = new UsuarioDto(null, "Eduardo", "Varela", "Delgado", "li.eduardo08@gmail.com", "A", "E", "evd", "evd", "I", "116590610");
        UsuarioDto user2 = new UsuarioDto(null, "Jesus", "varela", "Alfaro", "liasdas.com", "A", "E", "je", "je", "I", "116590324");
        PacienteDto pac = new PacienteDto(null, "117050374", "F", LocalDate.now(), "Cervantes Chacon", "Susana", "susi0326@gmail.com", "87873193");
        med.setMedUsuId(user2);
        AppContext.getInstance().set("bMedico", med);
        AppContext.getInstance().set("bUser", user);
        AppContext.getInstance().set("bPaci", pac);
    }

    @FXML
    private void evtEspecialidad(ActionEvent event) {
        FlowController.getInstance().goView("MantEspecialidad");
    }

    @FXML
    private void evtReportes(ActionEvent event) {
           FlowController.getInstance().goView("Reporte");

    }
}
