/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.EspecialidadDto;
import clinicauna.model.MedicoDto;
import clinicauna.service.EspecialidadService;
import clinicauna.service.MedicoService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class BuscarEspecialidadController extends Controller implements Initializable {

    @FXML
    private TableColumn<EspecialidadDto,String> tcNombre;
    @FXML
    private JFXTextField tfNombre;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private TableView<EspecialidadDto> tbEspecialidad;
    
    MedicoService medicoService;
    EspecialidadService especialidad;
    ObservableList<EspecialidadDto> especialidades;
    Respuesta respuesta;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
        tcNombre.setCellValueFactory(x->new SimpleStringProperty(x.getValue().getEspNombre()));   
    }    


    @FXML
    private void evtBuscar(ActionEvent event) 
    {
        Respuesta res=especialidad.getEsp();
        List< EspecialidadDto> esp = (List<EspecialidadDto>)res.getResultado("Especialidad");
        if(res.getEstado())
        {
            especialidades= FXCollections.observableArrayList(esp);
            tbEspecialidad.setItems(especialidades);
        }
        else
        {
            new Mensaje().showModal(Alert.AlertType.ERROR, "", getStage(),FlowController.getInstance().getLanguage().getString("msjBEsp"));

        }
        
    }

    @FXML
    private void evtAceptar(ActionEvent event) 
    {
       if(tbEspecialidad.getSelectionModel().getSelectedItem() != null)
       {
           EspecialidadDto e = tbEspecialidad.getSelectionModel().getSelectedItem();
           AppContext.getInstance().set("EspB",e);
           getStage().close();
       }
    }

    @Override
    public void initialize() 
    {
        especialidad = new EspecialidadService();
        medicoService = new MedicoService();
        
        tbEspecialidad.getItems().clear();
        tfNombre.clear();
    }
    
}
