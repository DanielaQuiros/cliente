/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.CitaDto;
import clinicauna.model.MedicoDto;
import clinicauna.model.PacienteDto;
import clinicauna.model.UsuarioDto;
import clinicauna.service.CitaService;
import clinicauna.service.PacienteService;
import clinicauna.util.AppContext;
import clinicauna.util.Card;
import clinicauna.util.Correo;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class MantCitaController extends Controller implements Initializable {

    private static final Logger LOG = Logger.getLogger(MantCitaController.class.getName());

    @FXML
    private JFXButton bntBuscar;
    @FXML
    private JFXButton bntGuardar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private JFXButton btnAtras;
    @FXML
    private JFXTextField txtPaNombre;
    @FXML
    private JFXTextField txtPaApellidos;
    @FXML
    private JFXTextField txtPaCedula;
    @FXML
    private JFXTextField txtCorreo;
    @FXML
    private JFXTextField txtTelefono;
    @FXML
    private JFXDatePicker dpFecha;
    // private JFXTimePicker tpHora;
    @FXML
    private JFXTextField txtMedico;
    @FXML
    private JFXComboBox<Integer> cmbEspacios;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXTextArea txaMotivo;
    @FXML
    private JFXCheckBox cxbActualizarInfo;
    @FXML
    private JFXButton btnCargarPaciente;
    @FXML
    private JFXRadioButton rbDisponible;
    @FXML
    private ToggleGroup Estado;
    @FXML
    private JFXRadioButton rbAusente;
    @FXML
    private JFXRadioButton rbCancelada;
    @FXML
    private JFXRadioButton rbAtendida;
    @FXML
    private JFXButton btnNuevoPaciente;
    @FXML
    private JFXComboBox<LocalTime> cbHoras;

    MedicoDto medico;
    PacienteDto paciente;
    UsuarioDto usuario;
    CitaDto cita;
    List<LocalTime> listHras;
    Correo correo;

    ResourceBundle idioma;
    @FXML
    private JFXButton btnConsulta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        idioma = FlowController.getInstance().getLanguage();
        rbAtendida.setUserData("A");
        rbAusente.setUserData("N");
        rbCancelada.setUserData("C");
        rbDisponible.setUserData("P");
        Formato();
        cmbEspacios.setItems(FXCollections.observableArrayList(4, 3, 2, 1));
    }

    @Override
    public void initialize() {
        cmbEspacios.setValue(1);
        iniVariables();
        generar();
        correo = new Correo("unaprogra32018@gmail.com", "una32018");

        if (((UsuarioDto) AppContext.getInstance().get("usuarioActual")).getUsuId().equals("M")) {
            btnConsulta.setVisible(true);
            btnConsulta.setDisable(false);
        } else {
            btnConsulta.setVisible(false);
            btnConsulta.setDisable(true);
        }
    }

    public void iniVariables() {
        cita = new CitaDto();
        cita = (CitaDto) AppContext.getInstance().get("citaClick");
        medico = cita.getCitMedId();
        paciente = new PacienteDto();
        usuario = (UsuarioDto) AppContext.getInstance().get("usuarioActual");
        txaMotivo.clear();
        binCita();
    }

    public void Formato() {
        txtCorreo.setTextFormatter(Formato.getInstance().maxLengthFormat(60));
        txtTelefono.setTextFormatter(Formato.getInstance().integerFormat());
    }

    public void binCita() {
        if (cita.getPaciente() != null) {
            txtPaNombre.textProperty().bindBidirectional(cita.paciente.pacNombre);
            txtPaApellidos.textProperty().bindBidirectional(cita.paciente.pacApellidos);
            txtPaCedula.textProperty().bindBidirectional(cita.paciente.pacCedula);
            txtTelefono.textProperty().bindBidirectional(cita.paciente.PacTelefono);
            txtCorreo.textProperty().bindBidirectional(cita.paciente.pacCorreo);
        }
        txtMedico.textProperty().bind(medico.getMedUsuId().usuNombre.concat(" ").concat(medico.getMedUsuId().usuPapellido));
        txtUsuario.textProperty().bind(usuario.usuNombre.concat(" ").concat(usuario.usuPapellido));
        dpFecha.valueProperty().bindBidirectional(cita.citFecha);
        if (cita.getCitMotivo() != null) {
            txaMotivo.textProperty().bindBidirectional(cita.citMotivo);
        }
        cbHoras.setValue(LocalTime.parse(cita.getCitHora()));
        cmbEspacios.setValue(cita.getCitEspacios());

    }

    public void unbinCita() {
        txtPaNombre.textProperty().unbindBidirectional(paciente.pacNombre);
        txtPaApellidos.textProperty().unbindBidirectional(paciente.pacApellidos);
        txtPaCedula.textProperty().unbindBidirectional(paciente.pacCedula);
        txtTelefono.textProperty().unbindBidirectional(paciente.PacTelefono);
        txtCorreo.textProperty().unbindBidirectional(paciente.pacCorreo);
        txtMedico.textProperty().bind(medico.getMedUsuId().usuNombre);
        txtUsuario.textProperty().bind(usuario.usuNombre);
    }

    public void nuevaCita() {
        unbinCita();
        iniVariables();
        binCita();
        txaMotivo.clear();
        cxbActualizarInfo.setSelected(Boolean.FALSE);
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarCitas", getStage(), Boolean.FALSE);
        if (((CitaDto) AppContext.getInstance().get("citaBusqueda")) != null) {
            cita = (CitaDto) AppContext.getInstance().get("citaBusqueda");
            binCita();
        }
    }

    @FXML
    private void evtGuardar(ActionEvent event) {
        try {
            if (validar()) {
                if (cita.getPaciente().getPacId() != null) {
                    if (cita.getCitId() != null || !dpFecha.getValue().isBefore(LocalDate.now())) {
                        cita.setCitMotivo(txaMotivo.getText());
                        cita.setCitEstado(Estado.getSelectedToggle().getUserData().toString());
                        cita.setCitTelefono(txtTelefono.getText());
                        cita.setCitCorreo(txtCorreo.getText());
                        cita.setCitHora(cbHoras.getValue().toString());
                        cita.setCitEspacios(cmbEspacios.getValue());
                        if (cxbActualizarInfo.isSelected()) {
                            cita.paciente.setPacTelefono(txtTelefono.getText());
                            cita.paciente.setPacCorreo(txtCorreo.getText());
                            Respuesta resp = new PacienteService().guardarPaciente(cita.paciente);
                            if (!resp.getEstado()) {
                                LOG.log(Level.SEVERE, "Ocurrio un error actualizando el paciente." + resp.getMensaje());
                            }
                        }
                        Respuesta resp = new CitaService().guardarcita(cita);
                        enviarCorreo(cita);
                        if (!resp.getEstado()) {
                            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Cita", getStage(), idioma.getString("msjCitaExito"));
                            cita = new CitaDto();
                            cita = (CitaDto) resp.getResultado("Cita");
                            binCita();
                            AppContext.getInstance().set("citaClick", null);
                        }
                    } else {
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar cita", getStage(), idioma.getString("msjFechaError"));
                    }
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar cita", getStage(), idioma.getString("selecPaciente"));
                }
            } else {
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar cita", getStage(), idioma.getString("msjCitNoCampos"));
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la cita.");
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar cita", getStage(), idioma.getString("mjsCita"));
        }
    }

    @FXML
    private void evtEliminar(ActionEvent event) {
        Respuesta resp = new CitaService().eliminarCita(cita.getCitId());
        if (resp.getEstado()) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar Cita", getStage(), idioma.getString("msjCitEliminada"));
            cita = new CitaDto();
            binCita();
        }
    }

    @FXML
    private void evtLimpiar(ActionEvent event) {
        nuevaCita();
        campos();
    }

    @FXML
    private void evtCargarPaciente(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarPacientes", getStage(), Boolean.FALSE);
        cita.setPaciente((PacienteDto) AppContext.getInstance().get("buscarPaciente"));
        binCita();
        System.out.println(paciente);
    }

    @FXML
    private void evtAtras(ActionEvent event) {
        AppContext.getInstance().set("vengo", "cita");
        FlowController.getInstance().goView("Agenda");
    }

    @FXML
    private void evtNuevoPaciente(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("RegPacientes", getStage(), Boolean.FALSE);
        cita.setPaciente((PacienteDto) AppContext.getInstance().get("buscarPaciente"));
        binCita();
    }

    @FXML
    private void evtFechas(ActionEvent event) {
        System.out.println(dpFecha.getValue());
        generar();

    }

    public void generar() {
        listHras = new ArrayList();
        Integer suma = (60 / medico.getMedEspaciosPorHora());
        //Integer suma = (60 / 4);
        LocalTime hora = medico.getMedIniciojornada().minusMinutes(suma);
        LocalTime fin = medico.getMedFinjornada();
        while (hora.isBefore(fin.minusMinutes(suma))) {
            hora = hora.plusMinutes(suma);
            listHras.add(hora);

        }
        remover(listHras);
        cbHoras.setItems(FXCollections.observableList(listHras));
        if (cita.getCitHora() != null) {
            cbHoras.setValue(LocalTime.parse(cita.getCitHora()));
        }
    }

    public void remover(List<LocalTime> list) {
        try {
            Respuesta resp = new CitaService().allcitas(medico.getMedId(), dpFecha.getValue().toString());
            if (resp.getEstado()) {
                List<CitaDto> citas = (List<CitaDto>) resp.getResultado("citas");
                for (CitaDto cit : citas) {
                    list.remove(LocalTime.parse(cit.getCitHora()));
                }
                list.add(0, LocalTime.parse(cita.getCitHora()));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Boolean campos() { /// CREO QUE ESTE NO SE USA
        Boolean si = Boolean.TRUE;
        if (!cmbEspacios.getValue().equals(1)) {
            si = Boolean.FALSE;
            LocalTime actual = cbHoras.getValue();
            Integer dist = (60 / medico.getMedEspaciosPorHora());
            for (int i = 0; i < listHras.size(); i++) {
                if (listHras.get(i).equals(actual)) {
                    System.out.println(actual);
                    for (int j = 1; j < cmbEspacios.getValue(); j++) {
                        if (i + j < listHras.size()) {
                            if (listHras.get(i + j).equals(actual.plusMinutes(dist * j))) {
                                si = Boolean.TRUE;
                            }
                        } else {
                            System.out.println("Mayor capacidad");
                            si = Boolean.FALSE;
                            j = 10;
                        }
                    }
                }
            }
        }
        System.out.println("Se puede: " + si);
        return si;
    }

    @FXML
    private void evt_Consulta(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("MantConsulta", getStage(), Boolean.FALSE);
    }

    public void enviarCorreo(CitaDto cita) {
        Thread thread = new Thread(() -> {
            correo.EnviarTexto(cita.getPaciente().getPacCorreo(), "Confirmacion de cita",
                    "<table style=\"max-width:600px;padding:10px;margin:0 auto;border-collapse:collapse\">"
                    + "<tbody><tr>"
                    + "<td style=\"background-color:#E1F5FE\">"
                    + "<div style=\"color:#34495e;margin:4% 10% 2%;text-align:justify;font-family:candara\">"
                    + "<h2 style=\"color:#03366;margin:0 0 7px\">Cita</h2>"
                    + "<p style=\"margin:2px;font-size:15px;font-family:roboto\">"
                    + "Se le informa que su cita ha sido agendada para el día <br>"
                    + cita.getCitFecha().toString() + " a las : " + cita.getCitHora()
                    + "</p>"
                    + "<p style=\"color:#b3b3b3;font-size:12px;text-align:center;margin:30px;font-family:roboto 0 0\">ClinicaUNA</p>"
                    + "<p></p></div>"
                    + "</td>"
                    + "</tr>"
                    + "</tbody></table>"
            );
        }
        );
        thread.start();
    }

    public Boolean validar() {
        try {
            List<Card> revisar = (List<Card>) AppContext.getInstance().get("ListCitas");
            Boolean si = Boolean.TRUE;
            String hActu = cbHoras.getValue().toString();
            Integer esp = cmbEspacios.getValue();
            if (esp > 1) {
                for (int i = 0; i < revisar.size(); i++) {
                    if (revisar.get(i).getCita().getCitHora().equals(hActu)) {
                        for (int j = 1; j < esp; j++) {
                            if (revisar.get(i + j).getCita().getCitId() == null) {//si la siguiente es nula
                                si = Boolean.TRUE;
                            } else { //si la siguiente es igual a la actual
                                if (revisar.get(i + j).getCita().getCitId() == cita.getCitId()) {
                                    si = Boolean.TRUE;
                                } else {//si la siguiente esta llena y no es la mimsma
                                    si = Boolean.FALSE;
                                    j = 100;
                                }
                            }
                        }
                        i = 1000;
                    }
                }
            }
            return si;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }
}//fin clase
