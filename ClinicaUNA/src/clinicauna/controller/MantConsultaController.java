/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.CitaDto;
import clinicauna.model.ConsultaDto;
import clinicauna.model.ExpedienteDto;
import clinicauna.service.ConsultaService;
import clinicauna.service.ExpedienteService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Formato;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class MantConsultaController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfFrec;
    @FXML
    private JFXTextField tfPresion;
    @FXML
    private JFXTextField tfPeso;
    @FXML
    private JFXTextField tfTalla;
    @FXML
    private JFXTextField tfImc;
    @FXML
    private JFXTextArea txtAn;
    @FXML
    private JFXTextArea txtApuntes;
    @FXML
    private JFXTextField tfTemp;
    @FXML
    private JFXTextArea txtObser;
    @FXML
    private JFXTextArea txtExam;
    @FXML
    private JFXTextArea txtPlan;
    @FXML
    private JFXTextArea txTrat;
    @FXML
    private JFXTextField tfCita;
    @FXML
    private JFXTextField tfExp;
    
    ConsultaService consultaService;
    ExpedienteService expService;
    ConsultaDto consultaDto;
    List<Node> requeridos = new ArrayList<>();
    boolean buscar,buscarC,buscarE;
    CitaDto cita;
    @FXML
    private JFXButton btnAtras;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
       
    }
    @Override
    public void initialize() 
    {
        consultaDto = new ConsultaDto();
        consultaService = new ConsultaService();
        expService = new ExpedienteService();
        buscar = false;
        buscarC = false;
        buscarE = false;
        agregarRequeridos();
        nuevaConsulta();

        cita = new CitaDto();
        if(AppContext.getInstance().get("consultaActual") != null){
            consultaDto = (ConsultaDto) AppContext.getInstance().get("consultaActual");
            AppContext.getInstance().set("consultaActual", null);
            bindConsulta();
        }
        
        if(AppContext.getInstance().get("citaBusqueda") != null){
            cita = (CitaDto) AppContext.getInstance().get("citaBusqueda");
            tfCita.setText(cita.getCitEstado());
            Respuesta res = expService.getExpediente(cita.getPaciente().getPacId());
            ExpedienteDto exp = (ExpedienteDto)res.getResultado("expediente");
            tfExp.setText(exp.getId().toString());
            }
        
        if(AppContext.getInstance().get("expediente") != null){
            AppContext.getInstance().set("expediente", null);
            btnAtras.setVisible(true);
            btnAtras.setDisable(false);
        }else{
            btnAtras.setVisible(false);
            btnAtras.setDisable(true);
        }
        formato();
    }
    
    public void formato(){
        tfPeso.setTextFormatter(Formato.getInstance().integerFormat());
        tfFrec.setTextFormatter(Formato.getInstance().integerFormat());
        tfImc.setTextFormatter(Formato.getInstance().integerFormat());
        tfTalla.setTextFormatter(Formato.getInstance().integerFormat());
        tfPresion.setTextFormatter(Formato.getInstance().integerFormat());
        tfTemp.setTextFormatter(Formato.getInstance().integerFormat());
        
    }
    
    public void agregarRequeridos()
    {     
        requeridos.clear();
        requeridos.addAll(Arrays.asList(tfFrec, tfImc,tfPeso, tfPresion, tfTalla,tfTemp,txtApuntes,txtAn,txtExam,txtObser,txtPlan,txTrat, tfExp));
    }
    public String validarRequeridos(){
        Boolean validos = true; 
        String invalidos = "";
        for(Node node : requeridos){
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } 
            if (node instanceof JFXTextArea && !((JFXTextArea) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextArea) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextArea) node).getPromptText();
                }
                validos = false;
            } 
        }
        if (validos) 
        {
            return "";
        } 
        else 
        {
            return FlowController.getInstance().getLanguage().getString("camposRequeridos")+" [" + invalidos + "].";
        }
    }
    public void bindConsulta()
    {
        tfFrec.textProperty().bindBidirectional(consultaDto.conFrecuenciacardiaca);
        tfImc.textProperty().bindBidirectional(consultaDto.conImc);
        tfPeso.textProperty().bindBidirectional(consultaDto.conPeso);
        tfPresion.textProperty().bindBidirectional(consultaDto.conPresion);
        tfTalla.textProperty().bindBidirectional(consultaDto.conTalla);
        tfTemp.textProperty().bindBidirectional(consultaDto.conTemperatura);
        txTrat.textProperty().bindBidirectional(consultaDto.conTratamiento);
        txtAn.textProperty().bindBidirectional(consultaDto.conAnotaciones);
        txtApuntes.textProperty().bindBidirectional(consultaDto.conApuntes);
        txtExam.textProperty().bindBidirectional(consultaDto.conExamenes);
        txtObser.textProperty().bindBidirectional(consultaDto.conObservaciones);
        txtPlan.textProperty().bindBidirectional(consultaDto.conPlanatencion);
        
    }
    public void unbindConsulta()
    {
        tfFrec.textProperty().unbindBidirectional(consultaDto.conFrecuenciacardiaca);
        tfImc.textProperty().unbindBidirectional(consultaDto.conImc);
        tfPeso.textProperty().unbindBidirectional(consultaDto.conPeso);
        tfPresion.textProperty().unbindBidirectional(consultaDto.conPresion);
        tfTalla.textProperty().unbindBidirectional(consultaDto.conTalla);
        tfTemp.textProperty().unbindBidirectional(consultaDto.conTemperatura);
        txTrat.textProperty().unbindBidirectional(consultaDto.conTratamiento);
        txtAn.textProperty().unbindBidirectional(consultaDto.conAnotaciones);
        txtApuntes.textProperty().unbindBidirectional(consultaDto.conApuntes);
        txtExam.textProperty().unbindBidirectional(consultaDto.conExamenes);
        txtObser.textProperty().unbindBidirectional(consultaDto.conObservaciones);
        txtPlan.textProperty().unbindBidirectional(consultaDto.conPlanatencion);
    }
    public void nuevaConsulta()
    {
        unbindConsulta();
        consultaDto = new ConsultaDto();
        bindConsulta();
    }
    public void cargarConsulta()
    {
        if(buscar && AppContext.getInstance().get("consultaB") != null)
        {
            consultaDto = (ConsultaDto)AppContext.getInstance().get("consultaB");
            bindConsulta();
        }
        buscar = false;
    }
    public void cargarCita()
    {
        if(buscarC && AppContext.getInstance().get("citaBusqueda") != null)
        {
            cita = (CitaDto)AppContext.getInstance().get("citaBusqueda");
            tfCita.setText(cita.getCitEstado());
            Respuesta res = expService.getExpediente(cita.getPaciente().getPacId());
            ExpedienteDto exp = (ExpedienteDto)res.getResultado("expediente");
            tfExp.setText(exp.getTratamientos());
        }
        buscarC = false;
    }
    @FXML
    private void evtBuscar(ActionEvent event) 
    {
        FlowController.getInstance().goViewInWindowModal("buscarConsulta",getStage(),false);
        buscar = true;
        cargarConsulta();
    }

    @FXML
    private void evtGuardar(ActionEvent event) 
    {
        try
        { //#3f83a8
            String validos= validarRequeridos();
            if(!validos.isEmpty())
            {
                new Mensaje().showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("msjECon"), getStage(),validos);
            }
            else
            {
                bindConsulta();
                //CitaDto cita = (CitaDto)AppContext.getInstance().get("citaBusqueda");
                Respuesta res = expService.getExpediente(cita.getPaciente().getPacId());
                ExpedienteDto exp = (ExpedienteDto)res.getResultado("expediente");
                consultaDto.setCita(cita);
                consultaDto.setExpediente(exp);
                Respuesta respuesta = consultaService.guardarConsulta(consultaDto);
                new Mensaje().showModal(Alert.AlertType.INFORMATION,"",getStage(), FlowController.getInstance().getLanguage().getString("msjCon"));
            }
        }
        catch(Exception ex)
        {
            Logger.getLogger(MantConsultaController.class.getName()).log(Level.SEVERE, "Error guardando al guardar la consulta", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "", getStage(),FlowController.getInstance().getLanguage().getString("msjECon"));
        }
    }

    @FXML
    private void evtEliminar(ActionEvent event) 
    {
        consultaService = new ConsultaService();
        Respuesta res = new Respuesta();
        if(consultaDto.getConId() != null)
        {
            res= consultaService.eliminarConsulta(consultaDto.getConId());
            if(!res.getEstado())
            {
                new Mensaje().showModal(Alert.AlertType.WARNING, "", getStage(),FlowController.getInstance().getLanguage().getString("msjEGUsu"));
                System.out.println(res.getMensaje());
            }
            else
            {
                Limpiar();
                new Mensaje().showModal(Alert.AlertType.WARNING, "",getStage(), FlowController.getInstance().getLanguage().getString("msjEUsu"));
            }
        }
    }

    @FXML
    private void evtLimpiar(ActionEvent event) 
    {
        Limpiar();
    }


    public void Limpiar()
    {
        tfFrec.clear();
        tfImc.clear();
        tfPeso.clear();
        tfPresion.clear();
        tfTalla.clear();
        tfTemp.clear();
        txTrat.clear();
        txtAn.clear();
        txtApuntes.clear();
        txtExam.clear();
        txtPlan.clear();
        txtObser.clear();
        tfCita.clear();
        tfExp.clear();
    }

    @FXML
    private void evt_Cita(MouseEvent event) 
    {
        FlowController.getInstance().goViewInWindowModal("buscarCitas",getStage(),false);  
        buscarC = true;
        cargarCita();
    }

    @FXML
    private void btnAtras(ActionEvent event) {
        FlowController.getInstance().goView("HistorialConsultas");
    }
}
