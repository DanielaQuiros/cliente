/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.EspecialidadDto;
import clinicauna.model.MedicoDto;
import clinicauna.model.PacienteDto;
import clinicauna.service.ReporteService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class ReporteController extends Controller implements Initializable {

    @FXML
    private JFXButton btnGenAgenda;
    @FXML
    private JFXTextField txtPaciName;
    @FXML
    private JFXButton BtnPaciente;
    @FXML
    private JFXTextField txtPaciCedu;
    @FXML
    private JFXButton btnGenPaciente;
    @FXML
    private JFXTextField txtMedName;
    @FXML
    private JFXButton btnDoctor;
    @FXML
    private JFXTextField txtMedCarnet;
    @FXML
    private JFXDatePicker dpMedInicio;
    @FXML
    private JFXDatePicker dpMedFin;
    @FXML
    private JFXTextField txtEspecialidad;
    @FXML
    private JFXButton btnEspecialidad;
    @FXML
    private JFXButton btnGenEspecialidad;

    private MedicoDto medico;
    private EspecialidadDto especialidad;
    private PacienteDto paciente;
    private ResourceBundle idioma;
    @FXML
    private JFXCheckBox ckhabilitar;
    @FXML
    private JFXDatePicker dpEspecialidadIni;
    @FXML
    private JFXDatePicker dpEspecialidadFin;
    private JFXCheckBox cktodas;
    @FXML
    private JFXCheckBox ckUnica;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        idioma = FlowController.getInstance().getLanguage();
        especialidad = new EspecialidadDto();
        btnEspecialidad.setDisable(!ckUnica.isSelected());
        txtEspecialidad.setDisable(!ckUnica.isSelected());
    }
//Agenda......................................................

    @FXML
    private void evtGenAgenda(ActionEvent event) {
        try {
            LocalDate inico;
            LocalDate fin;
            if (!txtMedName.getText().isEmpty() && medico.getMedId() != null && dpMedInicio.getValue() != null) {
                inico = dpMedInicio.getValue();
                if (dpMedFin.getValue() == null || !ckhabilitar.isSelected()) {
                    fin = dpMedInicio.getValue();
                } else {
                    fin = dpMedFin.getValue();
                }
                if (!inico.isAfter(fin)) {
                    Respuesta resp = new ReporteService().rAgenda(medico.getMedId(), inico, fin);
                    if (resp.getEstado()) {
                        new Mensaje().showModal(Alert.AlertType.ERROR, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErReport") + resp.getMensajeInterno());
                    }
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErFechas"));
                }
            } else {
                new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjRequeridos"));
            }
        } catch (Exception e) {
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErReport") + e.getMessage());
        }

    }

    @FXML
    private void evtCargarMed(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarMedicos", getStage(), Boolean.FALSE);
        if ((MedicoDto) AppContext.getInstance().get("medico") != null) {
            medico = (MedicoDto) AppContext.getInstance().get("medico");
            System.out.println(medico.getMedUsuId().getUsuNombre());
            txtMedName.setText(medico.getMedUsuId().getUsuNombre());
            txtMedCarnet.setText(medico.getMedCarne());
        }
    }
//Paciente.....................................................

    @FXML
    private void evtCargarPac(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarPacientes", getStage(), Boolean.FALSE);
        if ((PacienteDto) AppContext.getInstance().get("buscarPaciente") != null) {
            paciente = (PacienteDto) AppContext.getInstance().get("buscarPaciente");
            System.out.println(paciente.getPacNombre());
            txtPaciName.setText(paciente.getPacNombre());
            txtPaciCedu.setText(paciente.getPacCedula());
        }
    }

    @FXML
    private void evtGenPaciente(ActionEvent event) {
        try {
            Respuesta resp = new ReporteService().rPaciente(paciente.getPacId());
            if (resp.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErReport") + resp.getMensajeInterno());
            }
        } catch (Exception e) {
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErReport") + e.getMessage());
        }

    }
//Especialidad.................................................

    @FXML
    private void evtCargarEspe(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarEspecialidad", getStage(), Boolean.FALSE);
        if ((EspecialidadDto) AppContext.getInstance().get("EspB") != null) {
            especialidad = ((EspecialidadDto) AppContext.getInstance().get("EspB"));
            txtEspecialidad.setText(especialidad.getEspNombre());
        }
    }

    @FXML
    private void evtGenEspecialidad(ActionEvent event) {
        try {
            String id = "%";
            if (ckUnica.isSelected()) {
                if (especialidad.getEspId() != null) {
                    id = especialidad.getEspId().toString();
                }
            }
            if (dpEspecialidadFin.getValue() != null && dpEspecialidadIni.getValue() != null) {
                LocalDate inico = dpEspecialidadIni.getValue();
                LocalDate fin = dpEspecialidadFin.getValue();
                if (!inico.isAfter(fin)) {
                    Respuesta resp = new ReporteService().rEspecialidad(id, dpEspecialidadIni.getValue(), dpEspecialidadFin.getValue());
                    if (resp.getEstado()) {
                        new Mensaje().showModal(Alert.AlertType.ERROR, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErReport") + resp.getMensajeInterno());
                    }
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErFechas"));
                }
            } else {
                new Mensaje().showModal(Alert.AlertType.INFORMATION, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjRequeridos"));
            }
        } catch (Exception e) {
            new Mensaje().showModal(Alert.AlertType.ERROR, idioma.getString("msjGenReporte"), getStage(), idioma.getString("msjErReport") + e.getMessage());
        }
    }

    @FXML
    private void evtHabilitar(ActionEvent event) {
        dpMedFin.setDisable(!ckhabilitar.isSelected());
    }

    @FXML
    private void evtUnica(ActionEvent event) {
        btnEspecialidad.setDisable(!ckUnica.isSelected());
        txtEspecialidad.setDisable(!ckUnica.isSelected());
    }

}
