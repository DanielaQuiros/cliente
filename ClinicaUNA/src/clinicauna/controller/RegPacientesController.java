/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.PacienteDto;
import clinicauna.service.PacienteService;
import clinicauna.util.AppContext;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;

/**
 * FXML Controller class
 *
 * @author Susana
 */
public class RegPacientesController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfNombre;
    @FXML
    private JFXTextField tfApellidos;
    @FXML
    private JFXTextField tfCedula;
    @FXML
    private JFXComboBox<String> cbGenero;
    @FXML
    private JFXTextField tfCorreo;
    @FXML
    private JFXTextField tfTelefono;
    @FXML
    private JFXDatePicker dpFechaNacimiento;
    @FXML
    private JFXButton bntBuscar;
    @FXML
    private JFXButton bntGuardar;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnLimpiar;
    
    PacienteDto paciente;
    List<Node> requeridos = new ArrayList<>();
    boolean buscar = false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        paciente = new PacienteDto();
    }    

    @Override
    public void initialize() {
        cbGenero.getItems().setAll(FlowController.getInstance().getLanguage().getString("femenino") , 
        FlowController.getInstance().getLanguage().getString("masculino"));
        agregarRequeridos();
        nuevoPaciente();
    }
    
    public void agregarRequeridos(){
        requeridos.clear();
        requeridos.addAll(Arrays.asList(tfNombre, tfApellidos, tfCedula, tfCorreo, tfTelefono, cbGenero, dpFechaNacimiento));
    }
    
    public String validarRequeridos(){
        Boolean validos = true; 
        String invalidos = "";
        for(Node node : requeridos){
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if(dpFechaNacimiento.getValue() != null && dpFechaNacimiento.getValue().isAfter(LocalDate.now())){
            invalidos = invalidos + FlowController.getInstance().getLanguage().getString("fechaInvalida");
        }
        if (validos) {
            return "";
        } else {
            return FlowController.getInstance().getLanguage().getString("camposRequeridos")+" [" + invalidos + "].";
        }
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        FlowController.getInstance().goViewInWindowModal("buscarPacientes", getStage(), false);
        buscar = true;
        cargarPaciente();
    }

    @FXML
    private void evtGuardar(ActionEvent event) {
        try{
            String invalidos = validarRequeridos();
            if(!invalidos.isEmpty()){
                new Mensaje().showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("guardarPaciente"), getStage(), invalidos);
            }else{
                PacienteService ps = new PacienteService();
                bindPaciente();
                Respuesta respuesta = ps.guardarPaciente(paciente);
                if(!respuesta.getEstado()){
                    new Mensaje().showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("guardarPaciente"), getStage(), FlowController.getInstance().getLanguage().getString("errorGuardarPaciente"));
                }else{
                    unbindPaciente();
                    paciente = (PacienteDto) respuesta.getResultado("Paciente");
                    bindPaciente();
                    if(paciente.getPacGenero().equals("F")){
                        cbGenero.getSelectionModel().select(0);
                    }else{
                        cbGenero.getSelectionModel().select(1);
                    }
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, FlowController.getInstance().getLanguage().getString("guardarPaciente"), getStage(), FlowController.getInstance().getLanguage().getString("pacienteGuardado"));
               AppContext.getInstance().set("buscarPaciente",paciente);
                }
            }
        }catch(Exception ex){
            Logger.getLogger(RegPacientesController.class.getName()).log(Level.SEVERE, "Error guardando el paciente", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, FlowController.getInstance().getLanguage().getString("guardarPaciente"), getStage(), FlowController.getInstance().getLanguage().getString("errorGuardarPaciente"));
        }

    }

    @FXML
    private void evtEliminar(ActionEvent event) {
        PacienteService ps = new PacienteService();
        Respuesta respuesta = null;
        if(paciente.getPacId() != null){
            respuesta = ps.eliminarPaciente(paciente.getPacId());
            if(!respuesta.getEstado()){
                new Mensaje().show(Alert.AlertType.WARNING, "", FlowController.getInstance().getLanguage().getString("eliminarPaciente"));
            }else{
            Limpiar();
            new Mensaje().show(Alert.AlertType.WARNING, "", FlowController.getInstance().getLanguage().getString("pacienteEliminado"));
            }
        }else
            new Mensaje().show(Alert.AlertType.WARNING, "", FlowController.getInstance().getLanguage().getString("seleccionarPaciente"));
    }

    @FXML
    private void evtLimpiar(ActionEvent event) {
        Limpiar();
    }
    
    public void Limpiar(){
        tfApellidos.clear();
        tfCedula.clear();
        tfCorreo.clear();
        tfNombre.clear();
        tfTelefono.clear();
        unbindPaciente();
        
        paciente = new PacienteDto();
        dpFechaNacimiento.setValue(null);
        cbGenero.getSelectionModel().clearSelection();
    }
    
    private void bindPaciente(){
        tfApellidos.textProperty().bindBidirectional(paciente.pacApellidos);
        tfNombre.textProperty().bindBidirectional(paciente.pacNombre);
        tfCedula.textProperty().bindBidirectional(paciente.pacCedula);
        tfCorreo.textProperty().bindBidirectional(paciente.pacCorreo);
        tfTelefono.textProperty().bindBidirectional(paciente.PacTelefono);
        dpFechaNacimiento.valueProperty().bindBidirectional(paciente.pacFechaNacimiento);  
        //cbGenero.valueProperty().bindBidirectional(paciente.pacGenero);
        if(cbGenero.getSelectionModel().getSelectedItem() != null){
            if(cbGenero.getSelectionModel().getSelectedIndex() == 0)
                paciente.setPacGenero("F");
            else
                paciente.setPacGenero("M");
        } 
}
    
    private void unbindPaciente(){
        tfApellidos.textProperty().unbindBidirectional(paciente.pacApellidos);
        tfNombre.textProperty().unbindBidirectional(paciente.pacNombre);
        tfCedula.textProperty().unbindBidirectional(paciente.pacCedula);
        tfCorreo.textProperty().unbindBidirectional(paciente.pacCorreo);
        tfTelefono.textProperty().unbindBidirectional(paciente.PacTelefono);
        dpFechaNacimiento.valueProperty().unbindBidirectional(paciente.pacFechaNacimiento);  
        //cbGenero.valueProperty().unbindBidirectional(paciente.pacGenero);
        cbGenero.getSelectionModel().clearSelection();
    }
    
    private void nuevoPaciente(){
        unbindPaciente();
        paciente = new PacienteDto();
        bindPaciente();
    }
    
    public void cargarPaciente(){
        if(buscar && AppContext.getInstance().get("buscarPaciente") != null){
            paciente = (PacienteDto) AppContext.getInstance().get("buscarPaciente");
            if(paciente.getPacGenero().equals("F")){
                cbGenero.getSelectionModel().select(0);
            }else{
                cbGenero.getSelectionModel().select(1);
            }
            bindPaciente();
            AppContext.getInstance().set("buscarPaciente", null);
        }
        buscar = false;
    }
}
