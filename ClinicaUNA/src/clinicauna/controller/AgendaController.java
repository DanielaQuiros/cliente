/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.controller;

import clinicauna.model.CitaDto;
import clinicauna.model.MedicoDto;
import clinicauna.service.CitaService;
import clinicauna.util.AppContext;
import clinicauna.util.Card;
import clinicauna.util.Correo;
import clinicauna.util.FlowController;
import clinicauna.util.Mensaje;
import clinicauna.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class AgendaController extends Controller implements Initializable {

    @FXML
    private JFXButton btnBuscar;
    @FXML
    private FlowPane fpAgenda;
    @FXML
    private Label lbMedico;
    @FXML
    private JFXDatePicker dpFecha;

    private MedicoDto Medico;
    private List<Card> listCitas;
    private Card pizarra;
    private Card ante;
    CitaDto citaGuardar = new CitaDto();
    Correo correo;
    ResourceBundle idioma;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        idioma = FlowController.getInstance().getLanguage();
        if (((String) AppContext.getInstance().get("vengo")).equals("menu")) { //error
            fpAgenda.getChildren().clear();
            Medico = new MedicoDto();
            lbMedico.setText("");
            dpFecha.setValue(null);
        } else {
            actualizarAgenda();
        }
        correo = new Correo("unaprogra32018@gmail.com", "una32018");
    }

    @FXML
    private void evtBuscar(ActionEvent event) {
        Long anterior = (Medico.getMedId() != null ? Medico.getMedId() : null);
        FlowController.getInstance().goViewInWindowModal("buscarMedicos", getStage(), Boolean.FALSE);
        if ((MedicoDto) AppContext.getInstance().get("medico") != null) {
            dpFecha.setDisable(false);
            Medico = (MedicoDto) AppContext.getInstance().get("medico");
            if (anterior != null && !anterior.equals(Medico.getMedId())) {
                fpAgenda.getChildren().clear();
            }
            lbMedico.setText(Medico.getMedUsuId().getUsuNombre());
        }
    }

    private void evtAtras(ActionEvent event) {
        FlowController.getInstance().goView("Menu");
    }

    public void cargarCita(List<CitaDto> cit) {
        fpAgenda.getChildren().clear();
        for (int i = 0; i < listCitas.size(); i++) {
            for (CitaDto citaDto : cit) {
                //System.out.println("cart" + card.getCita().getCitHora() + "->" + citaDto.getCitHora());
                if (listCitas.get(i).getCita().getCitHora().equals(citaDto.getCitHora())) {
                    for (int j = 0; j < citaDto.getCitEspacios(); j++) {
                        listCitas.get(i).setCita(citaDto);
                        listCitas.get(i).Style();
                        if (citaDto.getCitEspacios() > (j + 1)) {
                            i++;
                        }
                    }

                }
            }
        }
        fpAgenda.getChildren().addAll(listCitas);
        AppContext.getInstance().set("ListCitas", listCitas);

    }

    public void generar() {
        if (Medico.getMedIniciojornada() != null && Medico.getMedFinjornada() != null) {
            listCitas = new ArrayList();
            Integer suma = (60 / Medico.getMedEspaciosPorHora());
            // Integer suma = (60 / 4);
            LocalTime hora = Medico.getMedIniciojornada().minusMinutes(suma);
            LocalTime fin = Medico.getMedFinjornada();
            System.out.println("Inicio: " + Medico.getMedIniciojornada() + "\n" + "Fin: " + Medico.getMedFinjornada());
            //        LocalTime fin = hora.plusHours(8);
            while (hora.isBefore(fin.minusMinutes(suma))) {
                hora = hora.plusMinutes(suma);
                Card c1 = new Card();
                drag(c1);
                drop(c1);
                CitaDto cita = citaPrueba(hora);
                cita.setCitMedId(Medico);
                cita.setCitEstado("C");
                c1.setCita(cita);
                c1.tamano(110, 70);
                c1.Style();
                c1.go("MantCita", getStage());
                listCitas.add(c1);
            }
        } else {
            new Mensaje().show(Alert.AlertType.WARNING, "", FlowController.getInstance().getLanguage().getString("mjsMedHorario"));
        }
    }

    public CitaDto citaPrueba(LocalTime time) {
        CitaDto cit = new CitaDto();
        cit.setCitFecha(dpFecha.getValue());
        cit.setCitHora(time.toString());
        return cit;
    }

    @FXML
    private void evtCargar(ActionEvent event) {
        actualizarAgenda();
    }

    private void actualizarAgenda() {
        if (Medico.getMedId() != null && dpFecha.getValue() != null) {
            fpAgenda.getChildren().clear();
            Respuesta resp = new CitaService().allcitas(Medico.getMedId(), dpFecha.getValue().toString());
            if (resp.getEstado()) {
                List<CitaDto> citas = (List<CitaDto>) resp.getResultado("citas");
                Platform.runLater(() -> {
                    generar();
                    cargarCita(citas);
                });
            }
        } else {
            if (dpFecha.getValue() != null) {
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Cargar Citas", getStage(), idioma.getString("msjCargarMed"));
                dpFecha.setValue(null);
            }
        }

    }

    public void drag(Card btn) {
        btn.setOnDragDetected((MouseEvent event) -> {
            /* allow any transfer mode */
            pizarra = (Card) event.getSource();
            if (pizarra.getCita().getCitEstado().equalsIgnoreCase("P")) {
//                if (pizarra.getCita().getCitEspacios() == 1) {
                Dragboard db = btn.startDragAndDrop(TransferMode.COPY);
                ante = (Card) event.getSource();
                /* put a image on dragboard */
                ClipboardContent content = new ClipboardContent();
                content.putImage(pizarra.getFondo().getImage());
                db.setContent(content);
//                } else {
//                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "", getStage(), "Modificar dentro de la cita");
//                }
            }
            event.consume();
        });
    }

    public void drop(Card btn) {
        try {
            btn.setOnDragOver((DragEvent event) -> {
                Card car = (Card) event.getSource();
                if (car.getCita().getCitEstado().equalsIgnoreCase("C")) {
                    Dragboard db = event.getDragboard();
                    if (db.hasImage()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                    }
                }
                event.consume();
            });

            btn.setOnDragDropped((DragEvent event) -> {
                CitaDto citAnt = new CitaDto();
                CitaDto citAct = new CitaDto();
                Dragboard db = event.getDragboard();
                Card c = (Card) event.getSource();
                if (db.hasImage()) {
                    if (validar(pizarra, c)) {
                        System.out.println("si se puede");

                        String ant, act;
                        ant = pizarra.getCita().getCitHora();
                        act = c.getCita().getCitHora();
                        citAnt = pizarra.getCita();
                        citAnt.setCitHora(act);
                        citAct = c.getCita();
                        citAct.setCitHora(ant);

                        ante.setCita(citAct);
                        ante.Style();
                        c.setCita(citAnt);
                        c.Style();
                        Respuesta resp = new CitaService().guardarcita(citAnt);
                        if (resp.getEstado()) {
                            new Mensaje().showModal(Alert.AlertType.ERROR, "Error al mover cita", getStage(),FlowController.getInstance().getLanguage().getString("mjsCita"));
                        } else {
                            actualizarAgenda();
                        }
                    } else {
                        new Mensaje().showModal(Alert.AlertType.ERROR, "Error al mover cita", getStage(), FlowController.getInstance().getLanguage().getString("msjCitNoCampos"));

                    }

                } else {
                    event.setDropCompleted(false);
                }

                event.consume();
            });
        } catch (Exception e) {
        }
    }

    @FXML
    private void evt_Recordatorio(ActionEvent event) {
        if (dpFecha.getValue() != LocalDate.now()) {
            Respuesta respuesta = new CitaService().allcitas(Medico.getMedId(), dpFecha.getValue().toString());
            List<CitaDto> citas = (List<CitaDto>) respuesta.getResultado("citas");

            for (CitaDto c : citas) {
                if (c.getCitEstado().equals("P")) {
                    enviarCorreo(c);
                }
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Cargar Citas", getStage(), idioma.getString("msjRec"));
        }
    }

    public void enviarCorreo(CitaDto cita) {
        Thread thread = new Thread(() -> {
            correo.EnviarTexto(cita.getPaciente().getPacCorreo(), "Recordatorio de cita",
                    "<table style=\"max-width:600px;padding:10px;margin:0 auto;border-collapse:collapse\">"
                    + "<tbody><tr>"
                    + "<td style=\"background-color:#E1F5FE\">"
                    + "<div style=\"color:#34495e;margin:4% 10% 2%;text-align:justify;font-family:candara\">"
                    + "<h2 style=\"color:#03366;margin:0 0 7px\">Recordatorio de cita</h2>"
                    + "<p style=\"margin:2px;font-size:15px;font-family:roboto\">"
                    + "Se le recuerda que su cita es mañana a las <br> " + cita.getCitHora()
                    + "</p>"
                    + "<p style=\"color:#b3b3b3;font-size:12px;text-align:center;margin:30px;font-family:roboto 0 0\">ClinicaUNA</p>"
                    + "<p></p></div>"
                    + "</td>"
                    + "</tr>"
                    + "</tbody></table>"
            );
        }
        );
        thread.start();
    }

    public Boolean validar(Card origin, Card destino) {
        try {
            List<Card> revisar = listCitas;
            Integer esp = origin.getCita().getCitEspacios();
            Boolean si = Boolean.TRUE;
            if (esp > 1) {
                for (int i = 0; i < listCitas.size(); i++) {
                    if (revisar.get(i).getCita().getCitHora().equals(destino.getCita().getCitHora())) {
                        for (int j = 1; j < esp; j++) {
                            if (revisar.get(i + j).getCita().getCitId() == null) {//si la siguiente es nula
                                si = Boolean.TRUE;
                            } else {
                                if (revisar.get(i + j).getCita().getCitId() == origin.getCita().getCitId()) {
                                    si = Boolean.TRUE;
                                } else {//si la siguiente esta llena y no es la mimsma
                                    si = Boolean.FALSE;
                                    j = 100;
                                }
                            }
                        }
                    }
                }
            }
            return si;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }
}
