/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import clinicauna.util.LocalTimeAdapter;
import java.time.LocalTime;
import java.util.List;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author liedu
 */
@XmlRootElement(name = "MedicoDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class MedicoDto {

    @XmlTransient
    public SimpleStringProperty medId;
    @XmlTransient
    public SimpleStringProperty medCodigo;
    @XmlTransient
    public SimpleStringProperty medFolio;
    @XmlTransient
    public SimpleStringProperty medCarne;
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public SimpleObjectProperty<LocalTime> medIniciojornada;
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public SimpleObjectProperty<LocalTime> medFinjornada;
    @XmlTransient
    public SimpleStringProperty medEspecialidad;
    @XmlTransient
    public SimpleStringProperty medEspaciosPorHora;
    public UsuarioDto medUsuId;
    public EspecialidadDto medEspId;
    private List<CitaDto> citaList;
    

    public MedicoDto() {
        this.medId = new SimpleStringProperty();
        this.medCodigo = new SimpleStringProperty();
        this.medFolio = new SimpleStringProperty();
        this.medCarne = new SimpleStringProperty();
        this.medIniciojornada = new SimpleObjectProperty();
        this.medFinjornada = new SimpleObjectProperty();
        this.medEspecialidad = new SimpleStringProperty();
        this.medEspaciosPorHora = new SimpleStringProperty();
    }

    public MedicoDto(String medId, String medCodigo, String medFolio, String medCarne, String medIniciojornada, String medFinjornada, String medEspecialidad, String medEspHora) {
        this();
        this.medId.set(medId);
        this.medCodigo.set(medCodigo);
        this.medFolio.set(medFolio);
        this.medCarne.set(medCarne);
        this.medIniciojornada.set(LocalTime.parse(medIniciojornada));
        this.medFinjornada.set(LocalTime.parse(medFinjornada));
        this.medEspecialidad.set(medEspecialidad);
        this.medEspaciosPorHora.set(medEspHora);
    }

    public Long getMedId() {
        if (medId.get() != null && !medId.get().isEmpty()) {
            return Long.valueOf(medId.get());
        } else {
            return null;
        }
    }

    public void setMedId(Long medId) {
        this.medId.set(medId.toString());
    }

    public String getMedCodigo() {
        return medCodigo.get();
    }

    public void setMedCodigo(String medCodigo) {
        this.medCodigo.set(medCodigo);
    }

    public String getMedFolio() {
        return medFolio.get();
    }

    public void setMedFolio(String medFolio) {
        this.medFolio.setValue(medFolio);
    }

    public String getMedCarne() {
        return medCarne.get();
    }

    public void setMedCarne(String medCarne) {
        this.medCarne.set(medCarne);
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public  LocalTime getMedIniciojornada() {
        return medIniciojornada.get();
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public void setMedIniciojornada(LocalTime medIniciojornada) {
        this.medIniciojornada.set(medIniciojornada);
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public LocalTime getMedFinjornada() 
    {
        return medFinjornada.get();
    }
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    public void setMedFinjornada(LocalTime medFinjornada) {
        this.medFinjornada.set(medFinjornada);
    }

    public Integer getMedEspaciosPorHora() {
        if (medEspaciosPorHora.get() != null && !medEspaciosPorHora.get().isEmpty()) {
            return Integer.valueOf(medEspaciosPorHora.get());
        } else {
            return null;
        }
    }

    public void setMedEspaciosPorHora(Integer medEspHora) {
        this.medEspaciosPorHora.set(medEspHora.toString());
    }

    public UsuarioDto getMedUsuId() {
        return medUsuId;
    }

    public void setMedUsuId(UsuarioDto medUsuId) {
        this.medUsuId = medUsuId;
    }

    public List<CitaDto> getCitaList() {
        return citaList;
    }

    public void setCitaList(List<CitaDto> citaList) {
        this.citaList = citaList;
    }

    public EspecialidadDto getMedEspId() {
        return medEspId;
    }

    public void setMedEspId(EspecialidadDto medEspId) {
        this.medEspId = medEspId;
    }

}
