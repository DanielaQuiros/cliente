/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dani
 */
@XmlRootElement(name="UsuarioDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)

public class UsuarioDto 
{
   @XmlTransient
   public SimpleStringProperty usuId;
   @XmlTransient
   public SimpleStringProperty usuNombre;
   @XmlTransient
   public SimpleStringProperty usuPapellido;
   @XmlTransient
   public SimpleStringProperty usuSapellido;
   @XmlTransient
   public SimpleStringProperty usuCorreo;
   @XmlTransient
   public ObjectProperty<String> usuTipo;
   @XmlTransient
   public SimpleStringProperty usuIdioma;
   @XmlTransient
   public SimpleStringProperty usuContrasena;
   @XmlTransient
   public SimpleStringProperty usuUsuario;
   @XmlTransient
   public SimpleStringProperty usuEstado;
   @XmlTransient
   public SimpleStringProperty usuCedula;
   
   
   public UsuarioDto()
    {
        usuId=new SimpleStringProperty();
        usuNombre=new SimpleStringProperty();
        usuPapellido=new SimpleStringProperty();
        usuSapellido=new SimpleStringProperty();
        usuUsuario=new SimpleStringProperty();
        usuContrasena=new SimpleStringProperty();
        usuCorreo=new SimpleStringProperty();
        usuIdioma=new SimpleStringProperty();
        usuEstado=new SimpleStringProperty();
        usuTipo=new SimpleObjectProperty();
        usuCedula= new SimpleStringProperty();
    }

    public UsuarioDto(String usuId, String usuNombre, String usuPapellido, String usuSapellido, String usuCorreo, String usuTipo, String usuIdioma, String usuContrasena, String usuUsuario,String usuEstado, String usuCedula) {
        this();
        this.usuId.set(usuId);
        this.usuNombre.set(usuNombre);
        this.usuPapellido.set(usuPapellido);
        this.usuSapellido.set(usuSapellido);
        this.usuCorreo.set(usuCorreo);
        this.usuTipo.set(usuTipo);
        this.usuIdioma.set(usuIdioma);
        this.usuContrasena.set(usuContrasena);
        this.usuUsuario.set(usuUsuario);
        this.usuEstado.set(usuEstado);
        this.usuCedula.set(usuCedula);
    }
   
   
    public Long getUsuId() 
    {
        if(usuId.get()!=null && !usuId.get().isEmpty())
        {
           return Long.valueOf(usuId.get());
        }
        else
        {
           return null;
        }
    }

    public void setUsuId(Long usuId) 
    {
        this.usuId.set(usuId.toString());
    }

    public String getUsuNombre() 
    {
        return usuNombre.get();
    }

    public void setUsuNombre(String usuNombre) 
    {
        this.usuNombre.set(usuNombre);
    }
	
    public String getUsuPapellido() 
    {
        return usuPapellido.get();
    }

    public void setUsuPapellido(String usuPapellido) 
    {
        this.usuPapellido.set(usuPapellido);
    }
    public String getUsuSapellido() 
    {
        return usuSapellido.get();
    }

    public void setUsuSapellido(String usuSapellido) 
    {
        this.usuSapellido.set(usuSapellido);
    }
    public String getUsuUsuario() 
    {
        return usuUsuario.get();
    }

    public void setUsuUsuario(String usuUsuario) 
    {
        this.usuUsuario.set(usuUsuario);
    }
    public String getUsuContrasena() 
    {
        return usuContrasena.get();
    }

    public void setUsuContrasena(String usuContrasena) 
    {
        this.usuContrasena.set(usuContrasena);
    }
    public String getUsuCorreo() 
    {
        return usuCorreo.get();
    }

    public void setUsuCorreo(String usuCorreo) 
    {
        this.usuCorreo.set(usuCorreo);
    }
    public String getUsuIdioma() 
    {
        return usuIdioma.get();
    }

    public void setUsuIdioma(String usuIdioma) 
    {
        this.usuIdioma.set(usuIdioma);
    }
    public String getUsuEstado() 
    {
        return usuEstado.get();
    }

    public void setUsuEstado(String usuEstado) 
    {
        this.usuEstado.set(usuEstado.toUpperCase());
    }
    public String getUsuTipo() 
    {
        return usuTipo.get();
    }

    public void setUsuTipo(String usuTipo) 
    {
        this.usuTipo.set(usuTipo);
    }

    public String getUsuCedula() 
    {
        return usuCedula.get();
    }

    public void setUsuCedula(String usuCedula) 
    {
        this.usuCedula.set(usuCedula);
    }
    
}
