/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlTransient;



public class ConsultaDto 
{
    @XmlTransient
    public SimpleStringProperty conId;
    @XmlTransient
    public SimpleStringProperty conFrecuenciacardiaca;
    @XmlTransient
    public SimpleStringProperty conPresion;
    @XmlTransient
    public SimpleStringProperty conPeso;
    @XmlTransient
    public SimpleStringProperty conTalla;
    @XmlTransient
    public SimpleStringProperty conTemperatura;
    @XmlTransient
    public SimpleStringProperty conImc;
    @XmlTransient
    public SimpleStringProperty conAnotaciones;
    @XmlTransient
    public SimpleStringProperty conApuntes;
    @XmlTransient
    public SimpleStringProperty conPlanatencion;
    @XmlTransient
    public SimpleStringProperty conObservaciones;
    @XmlTransient
    public SimpleStringProperty conExamenes;
    @XmlTransient
    public SimpleStringProperty conTratamiento;
    public CitaDto cita;
    public ExpedienteDto expediente;

    public ConsultaDto() {
        this.conId = new SimpleStringProperty();
        this.conFrecuenciacardiaca = new SimpleStringProperty();
        this.conPresion = new SimpleStringProperty();
        this.conPeso = new SimpleStringProperty();
        this.conTalla = new SimpleStringProperty();
        this.conTemperatura = new SimpleStringProperty();
        this.conImc = new SimpleStringProperty();
        this.conAnotaciones = new SimpleStringProperty();
        this.conApuntes = new SimpleStringProperty();
        this.conPlanatencion = new SimpleStringProperty();
        this.conObservaciones = new SimpleStringProperty();
        this.conExamenes = new SimpleStringProperty();
        this.conTratamiento = new SimpleStringProperty();
    }
    
    

    public Long getConId() {
        if (conId.get() != null && !conId.get().isEmpty()) {
            return Long.valueOf(conId.get());
        } else {
            return null;
        }
    }

    public void setConId(Long conId) {
        this.conId.set(conId.toString());
    }

    public Integer getConFrecCardiaca() {
        if (conFrecuenciacardiaca.get() != null && !conFrecuenciacardiaca.get().isEmpty()) {
            return Integer.valueOf(conFrecuenciacardiaca.get());
        } else {
            return null;
        }
    }

    public void setConFrecCardiaca(Integer conFrecuenciacardiaca) {
        this.conFrecuenciacardiaca.set(conFrecuenciacardiaca.toString());
    }

    public Integer getConPresion() {

        return Integer.valueOf(conPresion.get());
    }

    public void setConPresion(Integer conPresion) {
        this.conPresion.set(conPresion.toString());
    }

    public Integer getConPeso() {
        
        if (conPeso.get() != null && !conPeso.get().isEmpty()) {
            return Integer.valueOf(conPeso.get());
        } else {
            return null;
        }
    }

    public void setConPeso(Integer conPeso) {
        this.conPeso.set(conPeso.toString());
    }

    public Integer getConTalla() {

       if (conTalla.get() != null && !conTalla.get().isEmpty()) {
            return Integer.valueOf(conTalla.get());
        } else {
            return null;
        }
    }

    public void setConTalla(Integer conTalla) {
        this.conTalla.set(conTalla.toString());
    }

    public Integer getConTemperatura() {
        if (conTemperatura.get() != null && !conTemperatura.get().isEmpty()) {
            return Integer.valueOf(conTemperatura.get());
        } else {
            return null;
        }
    }

    public void setConTemperatura(Integer conTemperatura) {
        this.conTemperatura.set(conTemperatura.toString());
    }

    public Long getConImc() {;
      if (conImc.get() != null && !conImc.get().isEmpty()) {
            return Long.valueOf(conImc.get());
        } else {
            return null;
        }

    }
    public void setConImc(Long conId) {
        this.conImc.set(conId.toString());
    }
    public String getConAnotaciones() {
        return conAnotaciones.get();
    }

    public void setConAnotaciones(String conAnotaciones) {
        this.conAnotaciones.set(conAnotaciones);
    }

    public String getConApuntes() {
        return conApuntes.get();
    }

    public void setConApuntes(String conApuntes) {
        this.conApuntes.set(conApuntes);
    }

    public String getConPlanatencion() {
        return conPlanatencion.get();
    }

    public void setConPlanatencion(String conPlanatencion) {
        this.conPlanatencion.set(conPlanatencion);
    }

    public String getConObservaciones() {
        return conObservaciones.get();
    }

    public void setConObservaciones(String conObservaciones) {
        this.conObservaciones.set(conObservaciones);
    }

    public String getConExamenes() {
        return conExamenes.get();
    }

    public void setConExamenes(String conExamenes) {
        this.conExamenes.set(conExamenes);
    }

    public String getConTratamiento() {
        return conTratamiento.get();
    }

    public void setConTratamiento(String conTratamiento) {
        this.conTratamiento.set(conTratamiento);
    }

    public CitaDto getCita() {
        return cita;
    }

    public void setCita(CitaDto cita) {
        this.cita = cita;
    }

    public ExpedienteDto getExpediente() {
        return expediente;
    }

    public void setExpediente(ExpedienteDto expediente) {
        this.expediente = expediente;
    }

    
    
}
