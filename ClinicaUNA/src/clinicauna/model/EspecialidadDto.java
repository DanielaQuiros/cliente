/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dani
 */
@XmlRootElement(name="EspecialidadDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)

public class EspecialidadDto 
{
    @XmlTransient
    public SimpleStringProperty espId;
    @XmlTransient
    public SimpleStringProperty  espNombre;
    @XmlTransient
    public List<MedicoDto> medicos;
    
    public EspecialidadDto()
    {
        espId = new SimpleStringProperty();
        espNombre = new SimpleStringProperty();
    }
    public Long getEspId() 
    {
        if(espId.get()!=null && !espId.get().isEmpty())
        {
           return Long.valueOf(espId.get());
        }
        else
        {
           return null;
        }
    }

    public void setEspId(Long espId) 
    {
        this.espId.set(espId.toString());
    }
    public String getEspNombre() 
    {
        return espNombre.get();
    }

    public void setEspNombre(String espNombre) 
    {
        this.espNombre.set(espNombre);
    }

    public List<MedicoDto> getMedicos() 
    {
        return medicos;
    }

    public void setMedicos(List<MedicoDto> medicos)
    {
        this.medicos = medicos;
    }
    
}
