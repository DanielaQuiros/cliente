/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import clinicauna.util.LocalDateAdapter;
import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Susana
 */
public class ArchivoDto {
    @XmlTransient
    public Long id;
    @XmlTransient
    public SimpleStringProperty ruta;
    @XmlTransient
    public SimpleStringProperty tipo;
    @XmlTransient
    public SimpleStringProperty anotaciones;
    @XmlTransient
    public SimpleStringProperty nombreExamen;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public ObjectProperty<LocalDate> fecha;
    @XmlTransient
    public ExpedienteDto expediente; 
    @XmlTransient
    private String archivo;

    public ArchivoDto(Long id, String ruta, String tipo, String anotaciones, String nombreExamen, LocalDate fecha, ExpedienteDto expediente) {
        this();
        this.id = id;
        this.ruta.set(ruta);
        this.tipo.set(tipo);
        this.anotaciones.set(anotaciones);
        this.nombreExamen.set(nombreExamen);
        this.fecha.set(fecha);
        this.expediente = expediente;
    }
    
    public ArchivoDto(){
        this.ruta = new SimpleStringProperty();
        this.tipo = new SimpleStringProperty();
        this.anotaciones = new SimpleStringProperty();
        this.nombreExamen = new SimpleStringProperty();
        this.fecha = new SimpleObjectProperty<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRuta() {
        return ruta.get();
    }

    public void setRuta(String ruta) {
        this.ruta.set(ruta);
    }

    public String getTipo() {
        return tipo.get();
    }

    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }

    public String getAnotaciones() {
        return anotaciones.get();
    }

    public void setAnotaciones(String anotaciones) {
        this.anotaciones.set(anotaciones);
    }

    public String getNombreExamen() {
        return nombreExamen.get();
    }

    public void setNombreExamen(String nombreExamen) {
        this.nombreExamen.set(nombreExamen);
    }

    public ExpedienteDto getExpediente() {
        return expediente;
    }

    public void setExpediente(ExpedienteDto expediente) {
        this.expediente = expediente;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getFecha() {
        return fecha.get();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setFecha(LocalDate fecha) {
        this.fecha.set(fecha);
    }
}
