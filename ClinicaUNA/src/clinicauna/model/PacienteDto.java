/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import clinicauna.util.LocalDateAdapter;

/**
 *
 * @author Susana
 */
public class PacienteDto {
    @XmlTransient
    public SimpleStringProperty pacId;
    @XmlTransient
    public SimpleStringProperty pacCedula;
    @XmlTransient
    public SimpleStringProperty pacGenero;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public ObjectProperty<LocalDate> pacFechaNacimiento;
    @XmlTransient
    public SimpleStringProperty pacApellidos;
    @XmlTransient
    public SimpleStringProperty pacNombre;
    @XmlTransient
    public SimpleStringProperty pacCorreo;
    @XmlTransient
    public SimpleStringProperty PacTelefono;
    
    public PacienteDto(){
        this.pacId = new SimpleStringProperty();
        this.PacTelefono = new SimpleStringProperty();
        this.pacApellidos = new SimpleStringProperty();
        this.pacCedula = new SimpleStringProperty();
        this.pacCorreo = new SimpleStringProperty();
        this.pacFechaNacimiento = new SimpleObjectProperty<>();
        this.pacGenero = new SimpleStringProperty();
        this.pacNombre = new SimpleStringProperty();
    }
    
    public void crearPaciente(){
        this.pacId = new SimpleStringProperty();
        this.PacTelefono = new SimpleStringProperty();
        this.pacApellidos = new SimpleStringProperty();
        this.pacCedula = new SimpleStringProperty();
        this.pacCorreo = new SimpleStringProperty();
        this.pacFechaNacimiento = new SimpleObjectProperty<>();
        this.pacGenero = new SimpleStringProperty();
        this.pacNombre = new SimpleStringProperty();
    }

    public PacienteDto(String pacId, String pacCedula, String pacGenero, LocalDate pacFechaNacimiento, String pacApellidos, String pacNombre, String pacCorreo, String PacTelefono) {
        crearPaciente();
        if(pacId != null){
            this.pacId.set(pacId);
        }
        this.pacCedula.set(pacCedula);
        this.pacGenero.setValue(pacGenero);
        this.pacFechaNacimiento.set(pacFechaNacimiento);
        this.pacApellidos.set(pacApellidos);
        this.pacNombre.set(pacNombre);
        this.pacCorreo.set(pacCorreo);
        this.PacTelefono.set(PacTelefono);
    }

    public Long getPacId() {
        if (pacId.get() != null && !pacId.get().isEmpty()) {
            return Long.valueOf(pacId.get());
        } else {
            return null;
        }
    }

    public void setPacId(String pacId) {
        this.pacId.set(pacId);
    }

    public String getPacCedula() {
        return pacCedula.get();
    }

    public void setPacCedula(String pacCedula) {
        this.pacCedula.set(pacCedula);
    }

    public String getPacGenero()  {
        return pacGenero.get();
    }

    public void setPacGenero(String pacGenero) {
        this.pacGenero.set(pacGenero);
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getPacFechaNacimiento() {
        return pacFechaNacimiento.get();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setPacFechaNacimiento(LocalDate pacFechaNacimiento) {
        this.pacFechaNacimiento.set(pacFechaNacimiento);
    }

    public String getPacApellidos() {
        return pacApellidos.get();
    }

    public void setPacApellidos(String pacApellidos) {
        this.pacApellidos.set(pacApellidos);
    }

    public String getPacNombre() {
        return pacNombre.get();
    }

    public void setPacNombre(String pacNombre) {
        this.pacNombre.set(pacNombre);
    }

    public String getPacCorreo() {
        return pacCorreo.get();
    }

    public void setPacCorreo(String pacCorreo) {
        this.pacCorreo.set(pacCorreo);
    }

    public String getPacTelefono() {
        return PacTelefono.get();
    }

    public void setPacTelefono(String PacTelefono) {
        this.PacTelefono.set(PacTelefono);
    } 
}
