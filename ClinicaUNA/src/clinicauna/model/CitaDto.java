/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import java.time.LocalDate;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import clinicauna.util.LocalDateAdapter;
import java.time.LocalTime;

/**
 *
 * @author liedu
 */
@XmlRootElement(name = "CitaDto")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class CitaDto {

    @XmlTransient
    public SimpleStringProperty citId;
    @XmlTransient
    public SimpleStringProperty citEstado;
    @XmlTransient
    public SimpleStringProperty citTelefono;
    @XmlTransient
    public SimpleStringProperty citCorreo;
    @XmlTransient
    public SimpleIntegerProperty citEspacios;
    @XmlTransient
    public SimpleObjectProperty<LocalDate> citFecha;
    @XmlTransient
    public SimpleObjectProperty<LocalTime> citHora;
    @XmlTransient
    public SimpleStringProperty citMotivo;
//    List<Expediente> iExpedienteList;
    public MedicoDto citMedId;
    public PacienteDto paciente;

    public CitaDto() {
        this.citId = new SimpleStringProperty();
        this.citEstado = new SimpleStringProperty();
        this.citTelefono = new SimpleStringProperty();
        this.citCorreo = new SimpleStringProperty();
        this.citEspacios = new SimpleIntegerProperty();
        this.citFecha = new SimpleObjectProperty();
        this.citHora = new SimpleObjectProperty();
        this.citMotivo = new SimpleStringProperty();
        this.citMedId = new MedicoDto();
        this.paciente = new PacienteDto();
    }

    public Long getCitId() {
        if (citId.get() != null && !citId.get().isEmpty()) {
            return Long.valueOf(citId.get());
        } else {
            return null;
        }
    }

    public void setCitId(Long citId) {
        this.citId.set(citId.toString());
    }

    public String getCitEstado() {
        return citEstado.get();
    }

    public void setCitEstado(String citEstado) {
        this.citEstado.set(citEstado);
    }

    public String getCitTelefono() {
        return citTelefono.get();
    }

    public void setCitTelefono(String citTelefono) {
        this.citTelefono.set(citTelefono);
    }

    public String getCitCorreo() {
        return citCorreo.get();
    }

    public void setCitCorreo(String citCorreo) {
        this.citCorreo.set(citCorreo);
    }

    public Integer getCitEspacios() {
        return citEspacios.get();
    }

    public void setCitEspacios(Integer citEspacios) {
        this.citEspacios.set(citEspacios);
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getCitFecha() {
        return citFecha.get();
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setCitFecha(LocalDate citFecha) {
        this.citFecha.set(citFecha);
    }

    public String getCitHora() {
        return citHora.get().toString();
    }

    public void setCitHora(String citHora) {
        this.citHora.set(LocalTime.parse(citHora));
    }

    public String getCitMotivo() {
        return citMotivo.get();
    }

    public void setCitMotivo(String citMotivo) {
        this.citMotivo.set(citMotivo);
    }

    public PacienteDto getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteDto pasiente) {
        this.paciente = pasiente;
    }

    public MedicoDto getCitMedId() {
        return citMedId;
    }

    public void setCitMedId(MedicoDto citMedId) {
        this.citMedId = citMedId;
    }

}
