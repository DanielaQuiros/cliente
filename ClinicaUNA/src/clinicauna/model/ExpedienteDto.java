/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.model;

import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Susana
 */
public class ExpedienteDto {
    @XmlTransient
    public Long id;
    @XmlTransient
    public SimpleStringProperty anteFamiliares;
    @XmlTransient
    public SimpleStringProperty antePalogicos;
    @XmlTransient
    public SimpleStringProperty alergias;
    @XmlTransient
    public SimpleStringProperty hospitalizaciones;
    @XmlTransient
    public SimpleStringProperty tratamientos;
    @XmlTransient
    public SimpleStringProperty operaciones;
    @XmlTransient
    public Long version;
    @XmlTransient
    public PacienteDto paciente;
    
    public ExpedienteDto(){
        this.anteFamiliares = new SimpleStringProperty();
        this.antePalogicos = new SimpleStringProperty();
        this.alergias = new SimpleStringProperty();
        this.hospitalizaciones = new SimpleStringProperty();
        this.tratamientos = new SimpleStringProperty();
        this.operaciones = new SimpleStringProperty();
    }

    public ExpedienteDto(Long id, String anteFamiliares, String antePalogicos, String alergias, String hospitalizaciones, String tratamientos, String operaciones, Long version) {
        this();
        this.id = id;
        this.anteFamiliares.set(anteFamiliares);
        this.antePalogicos.set(antePalogicos);
        this.alergias.set(alergias);
        this.hospitalizaciones.set(hospitalizaciones);
        this.tratamientos.set(tratamientos);
        this.operaciones.set(operaciones);
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnteFamiliares() {
        return anteFamiliares.get();
    }

    public void setAnteFamiliares(String anteFamiliares) {
        this.anteFamiliares.set(anteFamiliares);
    }

    public String getAntePalogicos() {
        return antePalogicos.get();
    }

    public void setAntePalogicos(String antePalogicos) {
        this.antePalogicos.set(antePalogicos);
    }

    public String getAlergias() {
        return alergias.get();
    }

    public void setAlergias(String alergias) {
        this.alergias.set(alergias);
    }

    public String getHospitalizaciones() {
        return hospitalizaciones.get();
    }

    public void setHospitalizaciones(String hospitalizaciones) {
        this.hospitalizaciones.set(hospitalizaciones);
    }

    public String getTratamientos() {
        return tratamientos.get();
    }

    public void setTratamientos(String tratamientos) {
        this.tratamientos.set(tratamientos);
    }

    public String getOperaciones() {
        return operaciones.get();
    }

    public void setOperaciones(String operaciones) {
        this.operaciones.set(operaciones);
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public PacienteDto getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteDto paciente) {
        this.paciente = paciente;
    }
    
}
