/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;

import clinicauna.model.ExpedienteDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Susana
 */
public class ExpedienteService {

    public Respuesta getExpediente(Long pacId) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("pacId", pacId);    //expediente/{pacId}
            Request request = new Request("ExpedienteController/expediente", "/{pacId}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            ExpedienteDto expedienteDto = (ExpedienteDto) request.readEntity(ExpedienteDto.class);
            return new Respuesta(true, "", "", "expediente", expedienteDto);
        } catch (Exception ex) {
            Logger.getLogger(ExpedienteService.class.getName()).log(Level.SEVERE, "Error obteniendo el expediente [" + pacId + "]", ex);
            return new Respuesta(false, "Error obteniendo el expediente.", "getExpediente " + ex.getMessage());
        }
    }

    public Respuesta guardarExpediente(ExpedienteDto expedienteDto) {
        try {
            Request request = new Request("ExpedienteController/guardarExpediente");
            request.post(expedienteDto);

            if (request.isError()) {
                System.out.println(request.getError());
                return new Respuesta(false, request.getError(), "");
            }

            ExpedienteDto expediente = (ExpedienteDto) request.readEntity(ExpedienteDto.class);
            return new Respuesta(true, "", "", "Expediente", expediente);
        } catch (Exception ex) {
            Logger.getLogger(ExpedienteService.class.getName()).log(Level.SEVERE, "Error guardando el expediente.", ex);
            return new Respuesta(false, "Error guardando el expediente.", "guardarExpediente " + ex.getMessage());
        }
    }

    public Respuesta eliminarExpediente(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ExpedienteController/eliminarExpediente", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(ExpedienteService.class.getName()).log(Level.SEVERE, "Error eliminando el expediente.", ex);
            return new Respuesta(false, "Error eliminando el expediente.", "eliminarExpediente " + ex.getMessage());
        }
    }
}
