/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;

import clinicauna.model.CitaDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author liedu
 */
public class CitaService {

    public CitaService() {
    }

    public Respuesta guardarcita(CitaDto citaDto) {
        try {
            Request request = new Request("CitaController/guardarCita");
            request.post(citaDto);

            if (request.isError()) {
                System.out.println(request.getError().toString());
                return new Respuesta(true, request.getError(), "");
            }

            CitaDto cita = (CitaDto) request.readEntity(CitaDto.class);
            return new Respuesta(false, "", "", "Cita", cita);
        } catch (Exception ex) {
            Logger.getLogger(CitaService.class.getName()).log(Level.SEVERE, "Error guardando cita.", ex);
            return new Respuesta(false, "Error guardando la cita.", "guardarCita " + ex.getMessage());
        }

    }

    public Respuesta allcitas(Long idMed, String fecha) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", idMed);
            parametros.put("fecha", fecha);
            Request request = new Request("CitaController/allCitas", "/{id}/{fecha}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<CitaDto> citas = new ArrayList<>();
            citas = (List<CitaDto>) request.readEntity(new GenericType<List<CitaDto>>() {
            });
            return new Respuesta(true, "", "", "citas", citas);
        } catch (Exception ex) {
            Logger.getLogger(CitaService.class.getName()).log(Level.SEVERE, "Error guardando cita.", ex);
            return new Respuesta(false, "Error obteniendo la cita.", "ObtenerCita " + ex.getMessage());
        }
    }

    public Respuesta eliminarCita(Long citId) {
        try {
            Map<String, Object> parametros = new HashMap();
            parametros.put("citId", citId);
            Request request = new Request("CitaController/eliminarCita", "/{citId}", parametros);
            request.delete();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(MedicoService.class.getName()).log(Level.SEVERE, "Error eliminando la cita [" + citId + "]", ex);
            return new Respuesta(false, "Error eliminando la cita.", "EliminarMedico " + ex.getMessage());
        }
    }
    
        public Respuesta citas(String Med, String fecha, String Paci) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("Med", "%"+Med+"%");
            parametros.put("fecha", fecha);
            parametros.put("Paci", "%"+Paci+"%");
            Request request = new Request("CitaController/getCitas", "/{Med}/{fecha}/{Paci}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<CitaDto> citas = new ArrayList<>();
            citas = (List<CitaDto>) request.readEntity(new GenericType<List<CitaDto>>() {
            });
            return new Respuesta(true, "", "", "citas", citas);
        } catch (Exception ex) {
            Logger.getLogger(CitaService.class.getName()).log(Level.SEVERE, "Error guardando cita.", ex);
            return new Respuesta(false, "Error obteniendo la cita.", "ObtenerCita " + ex.getMessage());
        }
    }
}
