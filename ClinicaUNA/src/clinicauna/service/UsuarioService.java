/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;

import clinicauna.model.UsuarioDto;
import clinicauna.util.Request;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import clinicauna.util.Respuesta;
import java.util.List;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Dani
 */
public class UsuarioService 
{
    public Respuesta getUsuario(String usuario, String contra) 
    {
        try 
        {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("usuario", usuario);
            parametros.put("contra", contra);
            Request request = new Request("UsuarioController/getUsuario", "/{usuario}/{contra}", parametros);
            request.get();

            if (request.isError()) 
            {
                return new Respuesta(false, request.getError(), "");
            }
            UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo el usuario [" + usuario + "]", ex);
            return new Respuesta(false, "Error obteniendo el usuario.", "getUsuario " + ex.getMessage());
        }
    }
    public Respuesta guardarUsuario(UsuarioDto usuarioDto)
    {
        try
        {
            Request request = new Request("UsuarioController/guardarUsuario");
            request.post(usuarioDto);

            if (request.isError()) 
            {
                System.out.println(request.getError());
                return new Respuesta(false, request.getError(), "");
                
            }
            UsuarioDto usuario = (UsuarioDto) request.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error guardando el usuario.", ex);
            return new Respuesta(false, "Error guardando el usuario.", "guardarUsuario " + ex.getMessage());
        }
    }
    public Respuesta recuperarUsuario(String correo,String contra)
    {
        try
        {
            Map<String,Object> parametros=new HashMap<>();
            parametros.put("correo",correo);
            parametros.put("contra",contra);
            
            Request request= new Request("UsuarioController/recuperarUsuario","/{correo}/{contra}",parametros);
            request.get();
            
            if(request.isError())
            {
                return new Respuesta(false,request.getError(),"");
            }
            return new Respuesta(true,"Exitoso","");
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo el usuario [" +correo+ "]", ex);
            return new Respuesta(false, "Error obteniendo el usuario.", "buscarUsuario " + ex.getMessage());
        }
    }
    public Respuesta activarUsuario(Long usuId)
    {
        try
        {
            Map<String,Object> parametros = new HashMap<>();
            parametros.put("usuId",usuId);
            Request request=new Request("UsuarioController/activarUsuario","/{usuId}",parametros);
            request.get();

            if (request.isError()) 
            {
              return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo el usuario [" +usuId+ "]", ex);
           return new Respuesta(false, "Error obteniendo el usuario.", "buscarUsuario " + ex.getMessage()); 
        }
    }
    public Respuesta eliminarUsuario(Long usuId)
    {
        try
        {
            Map<String,Object>parametros = new HashMap();
            parametros.put("usuId", usuId);
            Request request = new Request("UsuarioController/eliminarUsuario","/{usuId}",parametros);
            request.delete();
            
            if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            return new Respuesta(true,"","");
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error eliminando el usuario [" +usuId+ "]", ex);
           return new Respuesta(false, "Error eliminando el usuario.", "EliminarUsuario " + ex.getMessage()); 
        }
    }
    public Respuesta getUsuarios(String nombre,String cedula, String apellido)
    {
        try
        {
            Map<String,Object> parametros = new HashMap();
            parametros.put("nombre",nombre);
            parametros.put("cedula",cedula);
            parametros.put("apellido",apellido);
            Request request = new Request("UsuarioController/getUsuarios","/{nombre}/{cedula}/{apellido}",parametros);
            request.get();
            
             if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            List<UsuarioDto> usuarios = (List<UsuarioDto>) request.readEntity(new GenericType<List<UsuarioDto>>() {});
            return new Respuesta(true, "", "", "Usuarios", usuarios);
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo usuarios", ex);
           return new Respuesta(false, "Error obteniendo los usuarios.", "getUsuarios" + ex.getMessage()); 
        }
    }
    public Respuesta validarUsuario(String usuario)
    {
        try
        {
            Map<String,Object> parametros = new HashMap();
            parametros.put("usuUsuario",usuario);
            Request request = new Request("UsuarioController/validarUsuario","/{usuUsuario}",parametros);
            request.get();
            
             if(request.isError())
            {
                return new Respuesta(false, request.getError(),"No sirve");
            }
            UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);
            
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo usuarios", ex);
           return new Respuesta(false, "Error obteniendo los usuarios.", "validarUsuario" + ex.getMessage()); 
        }
    }
    public Respuesta getUsuId(Long usuario)
    {
        try
        {
            Map<String,Object> parametros = new HashMap();
            parametros.put("usuario",usuario);
            Request request = new Request("UsuarioController/getUsuId","/{usuario}",parametros);
            request.get();
            
             if(request.isError())
            {
                return new Respuesta(false, request.getError(),"No sirve");
            }
            UsuarioDto usuarioDto = (UsuarioDto) request.readEntity(UsuarioDto.class);
            return new Respuesta(true, "", "", "Usuario", usuarioDto);
            
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error obteniendo usuarios", ex);
           return new Respuesta(false, "Error obteniendo los usuarios.", "validarUsuario" + ex.getMessage()); 
        }
    }
}
