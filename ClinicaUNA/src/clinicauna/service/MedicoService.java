/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;

import clinicauna.model.MedicoDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Dani
 */
public class MedicoService 
{
   public Respuesta guardarMedico(MedicoDto medicoDto)
    {
        try
        {
            Request request = new Request("MedicoController/guardarMedico");
            request.post(medicoDto);

            if (request.isError()) 
            {
                System.out.println(request.getError());
                return new Respuesta(false, request.getError(), "");
                
            }
            MedicoDto medico = (MedicoDto) request.readEntity(MedicoDto.class);
            return new Respuesta(true, "", "", "Medico", medicoDto);
        }
        catch(Exception ex)
        {
            Logger.getLogger(MedicoService.class.getName()).log(Level.SEVERE, "Error guardando el medico.", ex);
            return new Respuesta(false, "Error guardando el medico.", "guardarMedico " + ex.getMessage());
        }
    } 
   
   public Respuesta eliminarMedico(Long medId)
   {
       try
       {
          Map<String,Object>parametros = new HashMap();
            parametros.put("medId", medId);
            Request request = new Request("MedicoController/eliminarMedico","/{medId}",parametros);
            request.delete();
            
            if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            return new Respuesta(true,"",""); 
       }
       catch(Exception ex)
       {
           Logger.getLogger(MedicoService.class.getName()).log(Level.SEVERE, "Error eliminando el medico [" +medId+ "]", ex);
           return new Respuesta(false, "Error eliminando el medico.", "EliminarMedico " + ex.getMessage());
       }
   }
   public Respuesta getMedico(String folio,String carne, String codigo)
    {
        try
        {
            Map<String,Object> parametros = new HashMap();
            parametros.put("folio",folio);
            parametros.put("carne",carne);
            parametros.put("codigo",codigo);
            Request request = new Request("MedicoController/getMedico","/{folio}/{carne}/{codigo}",parametros);
            request.get();
            
            if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            List<MedicoDto> medicos = (List<MedicoDto>) request.readEntity(new GenericType<List<MedicoDto>>() {});
            return new Respuesta(true, "", "", "Medicos", medicos);
        }
        catch(Exception ex)
        {
           Logger.getLogger(MedicoService.class.getName()).log(Level.SEVERE, "Error obteniendo medicos", ex);
           return new Respuesta(false, "Error obteniendo los medicos.", "getMedicos" + ex.getMessage()); 
        }
    }
   public Respuesta getEspecialidad(Long EspId)
   {
       try
       {
           Map<String,Object> parametros = new HashMap();
           parametros.put("EspId",EspId);
           Request request = new Request("MedicoController/getEspecialidad","/{EspId}",parametros);
           request.get();
           
           if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            List<MedicoDto> medicos = (List<MedicoDto>) request.readEntity(new GenericType<List<MedicoDto>>() {});
            return new Respuesta(true, "", "", "EspecialidadM", medicos);
       }
       catch(Exception ex)
       {
           Logger.getLogger(MedicoService.class.getName()).log(Level.SEVERE, "Error obteniendo las especilidades relacionadas al medico", ex);
           return new Respuesta(false, "Error obteniendo los medicos.", "getMedicos" + ex.getMessage()); 
       }
   }
   
   public Respuesta getMedico(Long id)
   {
       try
       {
           Map<String,Object> parametros = new HashMap();
           parametros.put("usu",id);
           Request request = new Request("MedicoController/getMedico","/{usu}",parametros);
           request.get();
           
           if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            return new Respuesta(true, "", "", "Medico", (MedicoDto) request.readEntity(MedicoDto.class));
       }
       catch(Exception ex)
       {
           Logger.getLogger(MedicoService.class.getName()).log(Level.SEVERE, "Error obteniendo las especilidades relacionadas al medico", ex);
           return new Respuesta(false, "Error obteniendo los medicos.", "getMedicos" + ex.getMessage()); 
       }
   }
}
