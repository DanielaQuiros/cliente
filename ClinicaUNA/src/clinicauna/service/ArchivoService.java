/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;

import clinicauna.model.ArchivoDto;
import clinicauna.model.ExpedienteDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Susana
 */
public class ArchivoService {

    public Respuesta getArchivo(Long expId) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idExp", expId); //ArchivoController/Archivos/{idExp}
            Request request = new Request("ArchivoController/Archivos", "/{idExp}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<ArchivoDto> archivos = new ArrayList<>();
            archivos = (List<ArchivoDto>) request.readEntity(new GenericType<List<ArchivoDto>>() {
            });
            return new Respuesta(true, "", "", "archivos", archivos);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoService.class.getName()).log(Level.SEVERE, "Error obteniendo el archivo [" + expId + "]", ex);
            return new Respuesta(false, "Error obteniendo el archivo.", "getArchivo " + ex.getMessage());
        }
    }
    
    public Respuesta archivo(Long archId) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("archId", archId);
            Request request = new Request("ArchivoController/Archivo", "/{archId}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            
            return new Respuesta(true, "", "", "Archivo", (ArchivoDto) request.readEntity(ArchivoDto.class));
        } catch (Exception ex) {
            Logger.getLogger(ArchivoService.class.getName()).log(Level.SEVERE, "Error obteniendo el archivo [" + archId + "]", ex);
            return new Respuesta(false, "Error obteniendo el archivo.", "getArchivo " + ex.getMessage());
        }
    }

    public Respuesta guardarArchivo(ArchivoDto archivoDto) {
        try {
            Request request = new Request("ArchivoController/guardarArchivo");
            request.post(archivoDto);

            if (request.isError()) {
                System.out.println(request.getError());
                return new Respuesta(false, request.getError(), "");
            }

            ArchivoDto archivo = (ArchivoDto) request.readEntity(ArchivoDto.class);
            return new Respuesta(true, "", "", "Archivo", archivo);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoService.class.getName()).log(Level.SEVERE, "Error guardando el archivo.", ex);
            return new Respuesta(false, "Error guardando el archivo.", "guardarArchivo " + ex.getMessage());
        }
    }

    public Respuesta eliminarArchivo(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("ArchivoController/eliminarArchivo", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(ArchivoService.class.getName()).log(Level.SEVERE, "Error eliminando el archivo.", ex);
            return new Respuesta(false, "Error eliminando el archivo.", "eliminarArchivo " + ex.getMessage());
        }
    }
}
