/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;

import clinicauna.util.Mensaje;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JComponent;
import javax.swing.JViewport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.view.JRViewer;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author liedu
 */
public class ReporteService {

    public Respuesta rAgenda(Long IdMed, LocalDate inicio, LocalDate fin) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("medico", IdMed);
            parametros.put("inicio", inicio.toString());
            parametros.put("fin", fin.toString());
            Request request = new Request("PacienteController/agenda", "/{medico}/{inicio}/{fin}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            String result = (String) request.readEntity(String.class);
            Base64.Decoder decoder = Base64.getDecoder();
            File file = new File("Reporte.xml");
            byte[] fileArray = decoder.decode(result);
            OutputStream put = new FileOutputStream(file);
            put.write(fileArray);
            put.close();
            InputStream input = new FileInputStream(file);
            input.read(fileArray);
            input.close();
            visor(new JRViewer("Reporte.xml", true));
            return new Respuesta(false, "", "", "reporte", "exitoso");
        } catch (Exception ex) {
            return new Respuesta(true, "Error obteniendo el usuario.", "getUsuario " + ex.getMessage());

        }
    }

    public Respuesta rPaciente(Long IdPac) {
        try {

            Map<String, Object> parametros = new HashMap<>();
            parametros.put("paciente", IdPac);
            Request request = new Request("PacienteController/paciente", "/{paciente}", parametros);
            request.get();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            String result = (String) request.readEntity(String.class);
            Base64.Decoder decoder = Base64.getDecoder();
            File file = new File("Reporte.xml");
            byte[] fileArray = decoder.decode(result);
            OutputStream put = new FileOutputStream(file);
            put.write(fileArray);
            put.close();
            InputStream input = new FileInputStream(file);
            input.read(fileArray);
            input.close();
            visor(new JRViewer("Reporte.xml", true));
            return new Respuesta(false, "", "", "reporte", "exitoso");
        } catch (Exception ex) {
            return new Respuesta(true, "Error obteniendo el usuario.", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta rEspecialidad(String IdEsp, LocalDate inicio, LocalDate fin) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("especialidad", IdEsp);
            parametros.put("inicio", inicio.toString());
            parametros.put("fin", fin.toString());
            Request request = new Request("PacienteController/especialidad", "/{especialidad}/{inicio}/{fin}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            String result = (String) request.readEntity(String.class);
            Base64.Decoder decoder = Base64.getDecoder();
            File file = new File("Reporte.xml");
            byte[] fileArray = decoder.decode(result);
            OutputStream put = new FileOutputStream(file);
            put.write(fileArray);
            put.close();
            InputStream input = new FileInputStream(file);
            input.read(fileArray);
            input.close();
            visor(new JRViewer("Reporte.xml", true));
            return new Respuesta(false, "", "", "reporte", "exitoso");
        } catch (Exception ex) {
            return new Respuesta(true, "Error obteniendo el usuario.", "getUsuario " + ex.getMessage());

        }
    }

    public void visor(JComponent jv) {
        try {
            SwingNode swingNode = new SwingNode();
            swingNode.setContent(jv);
            AnchorPane anchorPane = new AnchorPane();
            AnchorPane.setTopAnchor(swingNode, 0.0);
            AnchorPane.setBottomAnchor(swingNode, 0.0);
            AnchorPane.setLeftAnchor(swingNode, 0.0);
            AnchorPane.setRightAnchor(swingNode, 0.0);
            anchorPane.getChildren().add(swingNode);
            anchorPane.setMinSize(820, 600);
            Scene scene = new Scene(anchorPane);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            new Mensaje().showModal(Alert.AlertType.ERROR,"Viso",new Stage(), "sekas");
        }
            
    }
}
