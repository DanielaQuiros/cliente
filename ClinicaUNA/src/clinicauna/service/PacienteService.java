package clinicauna.service;

import clinicauna.model.PacienteDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

public class PacienteService {

    public Respuesta getPaciente(String cedula) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("paciente", cedula);
            Request request = new Request("PacienteController/getPaciente", "/{cedula}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            PacienteDto pacienteDto = (PacienteDto) request.readEntity(PacienteDto.class);
            return new Respuesta(true, "", "", "Paciente", pacienteDto);
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error obteniendo el paciente [" + cedula + "]", ex);
            return new Respuesta(false, "Error obteniendo el paciente.", "getPaciente " + ex.getMessage());
        }
    }

    public Respuesta getPaciente(String cedula, String nombre, String apellidos) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("cedula", cedula);
            parametros.put("nombre", nombre);
            parametros.put("apellidos", apellidos);
            Request request = new Request("PacienteController/pacientes", "/{cedula}/{nombre}/{apellidos}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<PacienteDto> pacientes = new ArrayList<>();
            pacientes = (List<PacienteDto>) request.readEntity(new GenericType<List<PacienteDto>>() {
            });
            return new Respuesta(true, "", "", "pacientes", pacientes);
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error obteniendo el paciente [" + cedula + "]", ex);
            return new Respuesta(false, "Error obteniendo el paciente.", "getPaciente " + ex.getMessage());
        }
    }

    public Respuesta guardarPaciente(PacienteDto pacienteDto) {
        try {
            Request request = new Request("PacienteController/guardarPaciente");
            request.post(pacienteDto);

            if (request.isError()) {
                System.out.println(request.getError());
                return new Respuesta(false, request.getError(), "");
            }

            PacienteDto paciente = (PacienteDto) request.readEntity(PacienteDto.class);
            return new Respuesta(true, "", "", "Paciente", paciente);
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error guardando el paciente.", ex);
            return new Respuesta(false, "Error guardando el paciente.", "guardarPaciente " + ex.getMessage());
        }
    }

    public Respuesta eliminarPaciente(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request("PacienteController/eliminarPaciente", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(PacienteService.class.getName()).log(Level.SEVERE, "Error eliminando el paciente.", ex);
            return new Respuesta(false, "Error eliminando el paciente.", "eliminarPaciente " + ex.getMessage());
        }
    }
}
