/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;
import clinicauna.util.Request;
import java.util.logging.Level;
import java.util.logging.Logger;
import clinicauna.util.Respuesta;
import clinicauna.model.ConsultaDto;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Dani
 */
public class ConsultaService 
{
    public Respuesta guardarConsulta(ConsultaDto consultaDto)
    {
        try
        {
            Request request = new Request("ConsultaController/guardarConsulta");
            request.post(consultaDto);

            if (request.isError()) 
            {
                System.out.println(request.getError());
                return new Respuesta(false, request.getError(), "");
                
            }
            ConsultaDto consulta = (ConsultaDto) request.readEntity(ConsultaDto.class);
            return new Respuesta(true, "", "", "Consulta", consultaDto);
        }
        catch(Exception ex)
        {
            Logger.getLogger(ConsultaService.class.getName()).log(Level.SEVERE, "Error guardando la consulta.", ex);
            return new Respuesta(false, "Error guardando la consulta.", "guardarConsulta " + ex.getMessage());
        }
    }
    public Respuesta eliminarConsulta(Long conId)
    {
        try
        {
            Map<String,Object>parametros = new HashMap();
            parametros.put("conId", conId);
            Request request = new Request("ConsultaController/eliminarConsulta","/{conId}",parametros);
            request.delete();
            
            if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            return new Respuesta(true,"","");
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error eliminando la consulta [" +conId+ "]", ex);
           return new Respuesta(false, "Error eliminando la consulta.", "EliminarConsulta " + ex.getMessage()); 
        }
    }
    public Respuesta getConsulta(Long citId)
    {
        try
        {
            Map<String,Object>parametros = new HashMap();
            parametros.put("citId", citId);
            Request request = new Request("ConsultaController/getConsulta","/{citId}",parametros);
            request.get();
            
            if(request.isError())
            {
                return new Respuesta(false, request.getError(),"Error al buscar la consulta");
            }
            ConsultaDto consulta = (ConsultaDto) request.readEntity(ConsultaDto.class);
            return new Respuesta(true, "", "", "Consultas",consulta);
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al buscar la consulta", ex);
           return new Respuesta(false, "Error al buscar la consulta.", "getConsulta " + ex.getMessage()); 
        }
    }
    public Respuesta getConsultas(Long pacId)
    {
        try
        {
            Map<String,Object>parametros = new HashMap();
            parametros.put("pacId", pacId);
            Request request = new Request("ConsultaController/getConsultas","/{pacId}",parametros);
            request.get();
            
            if(request.isError())
            {
                return new Respuesta(false, request.getError(),"Error al buscar la consulta");
            }
            List<ConsultaDto> consultas = (List<ConsultaDto>) request.readEntity(new GenericType<List<ConsultaDto>>() {});
            return new Respuesta(true, "", "", "Consultas",consultas);
        }
        catch(Exception ex)
        {
           Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al buscar la consulta", ex);
           return new Respuesta(false, "Error al buscar la consulta.", "getConsulta " + ex.getMessage()); 
        }
    }
}
