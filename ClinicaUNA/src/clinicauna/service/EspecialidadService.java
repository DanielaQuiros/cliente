/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicauna.service;

import clinicauna.model.EspecialidadDto;
import clinicauna.util.Request;
import clinicauna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Dani
 */
public class EspecialidadService 
{
    
    public Respuesta guardarEspecialidad(EspecialidadDto especialidadDto)
    {
        try
        {
            Request request = new Request("EspecialidadController/guardarEspecialidad");
            request.post(especialidadDto);

            if (request.isError()) 
            {
                System.out.println(request.getError());
                return new Respuesta(false, request.getError(), "");
                
            }
            EspecialidadDto especialidad = (EspecialidadDto) request.readEntity(EspecialidadDto.class);
            return new Respuesta(true, "", "", "Especialidad", especialidadDto);
        }
        catch(Exception ex)
        {
            Logger.getLogger(EspecialidadService.class.getName()).log(Level.SEVERE, "Error guardando la especialidad.", ex);
            return new Respuesta(false, "Error guardando el especialidad.", "guardarEspecialidad " + ex.getMessage());
        }
    }
    public Respuesta getEspecialidad(String nombre)
    {
        try
        {
            Map<String,Object> parametros = new HashMap();
            parametros.put("nombre","%"+nombre.toUpperCase()+"%");
            Request request = new Request("EspecialidadController/getEspecialidad","/{nombre}",parametros);
            request.get();
            
             if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            List<EspecialidadDto> especialidad = (List<EspecialidadDto>) request.readEntity(new GenericType<List<EspecialidadDto>>() {});
            return new Respuesta(true, "", "", "Especialidad", especialidad);
        }
        catch(Exception ex)
        {
           Logger.getLogger(EspecialidadService.class.getName()).log(Level.SEVERE, "Error obteniendo las especialidades", ex);
           return new Respuesta(false, "Error obteniendo las especialidades.", "getEspecialidad" + ex.getMessage()); 
        }
    }
    public Respuesta getEspecialidades()
    {
        try
        {
            Request request = new Request("EspecialidadController/getEspecialidades");
            request.get();
            
             if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            List<EspecialidadDto> especialidad = (List<EspecialidadDto>) request.readEntity(new GenericType<List<EspecialidadDto>>() {});
            return new Respuesta(true, "", "", "Especialidades", especialidad);
        }
        catch(Exception ex)
        {
           Logger.getLogger(EspecialidadService.class.getName()).log(Level.SEVERE, "Error obteniendo las especialidades", ex);
           return new Respuesta(false, "Error obteniendo las especialidades.", "getEspecialidades" + ex.getMessage()); 
        }
    }
    public Respuesta eliminarEspecialidad(Long espId)
    {
        try
        {
            Map<String,Object>parametros = new HashMap();
            parametros.put("espId", espId);
            Request request = new Request("EspecialidadController/eliminarEspecialidad","/{espId}",parametros);
            request.delete();
            
            if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            return new Respuesta(true,"","");
        }
        catch(Exception ex)
        {
            Logger.getLogger(EspecialidadService.class.getName()).log(Level.SEVERE, "Error eliminando el especialidad [" +espId+ "]", ex);
           return new Respuesta(false, "Error eliminando el especialidad.", "EliminarEspecialidad " + ex.getMessage()); 
        }
    }
    public Respuesta getEsp()
    {
        try
        {
            Request request = new Request("EspecialidadController/getEsp");
            request.get();
            
             if(request.isError())
            {
                return new Respuesta(false, request.getError(),"");
            }
            List<EspecialidadDto> especialidad = (List<EspecialidadDto>) request.readEntity(new GenericType<List<EspecialidadDto>>() {});
            return new Respuesta(true, "", "", "Especialidad", especialidad);
        }
        catch(Exception ex)
        {
           Logger.getLogger(EspecialidadService.class.getName()).log(Level.SEVERE, "Error obteniendo las especialidades", ex);
           return new Respuesta(false, "Error obteniendo las especialidades.", "getEspecialidades" + ex.getMessage()); 
        }
    }

}
