/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2.controller;

import clinicauna.util.AppContext;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import tarea2.Tarea2;
import tarea2.service.ActividadService;
import tarea2.service.PersonaService;
import tarea2.util.Controller;
import tarea2.util.EmailHandler;
import tarea2.util.LocalDateConvertion;
import tarea2.util.ScreenHandler;
import wstarea2.controller.ActividadDto;
import wstarea2.controller.ErrorInterno_Exception;
import wstarea2.controller.PersonaDto;
import wstarea2.controller.ProyectoDto;
import wstarea2.controller.SinResultados_Exception;

/**
 * FXML Controller class
 *
 * @author Dani
 */
public class ActividadesController extends Controller implements Initializable {

    @FXML
    private Label lb_Texto;
    @FXML
    private ImageView iv_Imagen;
    @FXML
    private TextField tf_Descrip;
    @FXML
    private TextField tf_Encargado;
    @FXML
    private DatePicker dp_Final;
    @FXML
    private DatePicker dp_Inicio;
    @FXML
    private DatePicker dt_InicioR;
    @FXML
    private DatePicker dt_FinalR;
    @FXML
    private ComboBox<String> cb_Estado;
    @FXML
    private Label lb_Advertencia;
    @FXML
    private ImageView iv_Advertencia;
    private Image imgWarning;
    private Image imgGuardado;
    public ActividadDto actDto;
	public ProyectoDto proyecto;
	public PersonaDto encargado;
	private boolean cambioEstado;

    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
		cambioEstado = false;
		encargado=null;
		proyecto=null;
		actDto=null;
        cb_Estado.getItems().addAll("Planificada","En curso","Postergada","Finalizada");
        imgWarning = new Image(Tarea2.class.getResourceAsStream("resources/warning.png"));
        imgGuardado = new Image(Tarea2.class.getResourceAsStream("resources/guardado.png"));
        iv_Imagen.setImage( imgWarning );
        iv_Imagen.setPreserveRatio(true);
        iv_Imagen.setFitWidth(40);
        
        tf_Descrip.textProperty().addListener((o, antes, ahora)->{
			lb_Texto.setText("SIN GUARDAR");
			iv_Imagen.setImage( imgWarning );
			iv_Advertencia.setVisible(false);
			lb_Advertencia.setVisible(false);
		});
        tf_Encargado.textProperty().addListener((o, antes, ahora)->{
			lb_Texto.setText("SIN GUARDAR");
			iv_Imagen.setImage( imgWarning );
			iv_Advertencia.setVisible(false);
			lb_Advertencia.setVisible(false);
		});
        dp_Final.valueProperty().addListener((o, antes, ahora)->{
			lb_Texto.setText("SIN GUARDAR");
			iv_Imagen.setImage( imgWarning );
			iv_Advertencia.setVisible(false);
			lb_Advertencia.setVisible(false);
		});
        dp_Inicio.valueProperty().addListener((o, antes, ahora)->{
			lb_Texto.setText("SIN GUARDAR");
			iv_Imagen.setImage( imgWarning );
			iv_Advertencia.setVisible(false);
			lb_Advertencia.setVisible(false);
		});
        dt_InicioR.valueProperty().addListener((o, antes, ahora)->{
			lb_Texto.setText("SIN GUARDAR");
			iv_Imagen.setImage( imgWarning );
			iv_Advertencia.setVisible(false);
			lb_Advertencia.setVisible(false); //
		});
        dt_FinalR.valueProperty().addListener((o, antes, ahora)->{
			lb_Texto.setText("SIN GUARDAR");
			iv_Imagen.setImage( imgWarning );
			iv_Advertencia.setVisible(false);
			lb_Advertencia.setVisible(false);
		});
        cb_Estado.getSelectionModel().selectedIndexProperty().addListener((o, antes,ahora)->{
			lb_Texto.setText("SIN GUARDAR");
			iv_Imagen.setImage( imgWarning );
			if ( !antes.equals( ahora ) ){
				cambioEstado = true;
			}
		});
		 
		
		  
    }   
	
    public void actualizar()
    {
        tf_Descrip.setText(actDto != null ? actDto.getDescripcion() :"");
		try{
			if(encargado==null){
		encargado= PersonaService.getPersonaID(actDto.getIdEncargado());
			}
		
		}catch(Exception ex){
			encargado=null;
		}
        tf_Encargado.setText(encargado != null ? encargado.getNombre() + " "+ encargado.getApellidos() :"");
        dp_Inicio.setValue(actDto != null ? LocalDateConvertion.toLocalDate(actDto.getFechaInicio()) : null);
        dp_Final.setValue(actDto != null ? LocalDateConvertion.toLocalDate(actDto.getFechaFinal()) : null);
        dt_FinalR.setValue(actDto != null ? LocalDateConvertion.toLocalDate(actDto.getFechaFinalReal()) : null);
        dt_InicioR.setValue(actDto != null ? LocalDateConvertion.toLocalDate(actDto.getFechaInicioReal()) : null);
        if(actDto != null){
			switch(actDto.getEstado()){
				case "P":
					cb_Estado.getSelectionModel().select( 0 );
				break;
				case "E":
					cb_Estado.getSelectionModel().select( 1 );
				break;
				case "PO":
					cb_Estado.getSelectionModel().select( 2 );
				break;
				case "F":
					cb_Estado.getSelectionModel().select( 3 );
				break;
			}
		}else{
			cb_Estado.getSelectionModel().select( 0 );
		}
    }
    @FXML
    private void evt_Guardar(ActionEvent event) 
    {
		boolean nuevo = actDto == null;
        if( proyecto!=null && encargado!=null && !tf_Descrip.getText().isEmpty() && !tf_Encargado.getText().isEmpty() && dp_Inicio.getValue() != null && dp_Final.getValue() != null)
        {
			if(nuevo)
            {
                actDto = new ActividadDto();
            }
			actDto.setOrden(0);
            actDto.setDescripcion(tf_Descrip.getText());
            actDto.setFechaInicio(LocalDateConvertion.toString(dp_Inicio.getValue()));
            actDto.setFechaFinal(LocalDateConvertion.toString(dp_Final.getValue()));
            actDto.setFechaInicioReal(LocalDateConvertion.toString(dt_InicioR.getValue()));
            actDto.setFechaFinalReal(LocalDateConvertion.toString(dt_FinalR.getValue()));
            actDto.setIdEncargado(encargado.getId()); 
           actDto.setIdProyecto(proyecto.getId()); 
		   actDto.setNombreEncargado( encargado.getNombre() + " " + encargado.getApellidos() );
            switch(cb_Estado.getSelectionModel().getSelectedIndex())
            {
                case 0:
                actDto.setEstado( "P" );
                break;
                case 1:
                actDto.setEstado( "E" );
                break;
                case 2:
                actDto.setEstado( "PO" );
                break;
                case 3:
                actDto.setEstado( "F" );
                break;
            }
            try 
            {
                actDto = ActividadService.guardarActividad(actDto);
                lb_Texto.setText("GUARDADO");
                iv_Imagen.setImage( imgGuardado );
                iv_Advertencia.setVisible(false);
                lb_Advertencia.setVisible( false );
				if(nuevo){
					proyecto.getActividades().add(actDto);
					enviarCorreo( "Nueva actividad" );
				}else if ( cambioEstado ){
					enviarCorreo( "Estado actualizado en actividad" );
				}
				cambioEstado = false;
            } 
            catch (ErrorInterno_Exception ex) 
            {
				if ( nuevo ){
					actDto=null;
				}
                lb_Advertencia.setText( "ERROR INTERNO" );
                iv_Advertencia.setImage( imgWarning );
                iv_Advertencia.setVisible(true);
                lb_Advertencia.setVisible( true );
            } 
            catch (SinResultados_Exception ex) 
            {
				if ( nuevo ){
					actDto=null;
				}
                lb_Advertencia.setText( "NO ENCONTRADO" );
                iv_Advertencia.setImage( imgWarning );
                iv_Advertencia.setVisible(true);
                lb_Advertencia.setVisible( true );
            }
        }
        else
        {
            lb_Advertencia.setText( "DATOS INCOMPLETOS" );
            iv_Advertencia.setImage( imgWarning );
            iv_Advertencia.setVisible(true);
            lb_Advertencia.setVisible( true );
		}
    }

	void enviarCorreo( String titulo ){
		Thread thread = new Thread( () -> {
			EmailHandler email = ( EmailHandler )AppContext.getInstance().get( "Email" );
			email.sendMessage( encargado.getCorreo(), titulo,
				"<div style=\"display: inline-block;\">" +
					"<div style=\"margin: 0; font-size: 2rem; background: rgb( 100, 200, 100 ); border-radius: 1rem 1rem 0 0; padding: 1rem; text-align: center; color: white; font-weight: bold;\">" + titulo + "</div>" +
					"<div style=\"background: rgb( 138, 255, 169 ); padding: 0.5rem; border-radius: 0 0 0.5rem 0.5rem;\">" +
						"<div style=\"font-size: 1rem;\"><span style=\"font-weight: bold;\">Descripción: </span>" + actDto.getDescripcion() + "</div>" +
						"<div style=\"font-size: 1rem;\"><span style=\"font-weight: bold;\">Fecha de inicio: </span>" + actDto.getFechaInicio() + "</div>" +
						"<div style=\"font-size: 1rem;\"><span style=\"font-weight: bold;\">Fecha final: </span>" + actDto.getFechaFinal() + "</div>" +
						"<div style=\"font-size: 1rem;\"><span style=\"font-weight: bold;\">Estado: </span>" + getEstado( actDto.getEstado() ) + "</div>" +
						"<div style=\"font-size: 1rem; mrgin: 0 1rem 0 0;\"><span style=\"font-weight: bold;\">Enviado el: </span>" + LocalDate.now() + " a las " + LocalTime.now() + "</div>" +
					"</div>" +
				"</div>"
			);
		} );
		thread.start();
	}
	
	private String getEstado( String estado ){
		String resultado = "Sin definir";
		switch ( estado ){
			case "P":
				resultado =  "Planificado";
			break;
			case "E":
				resultado = "En curso";
			break;
			case "PO":
				resultado = "Postergado";
			break;
			case "F":
				resultado = "Finalizado";
			break;
		}
		return resultado;
	}
	
    @FXML
    private void evt_Limpiar(ActionEvent event) 
    {
        actDto = null;
		encargado=null;
        iv_Imagen.setImage( imgWarning );
        iv_Advertencia.setVisible(false);
        lb_Advertencia.setVisible(false);
        lb_Texto.setText("SIN GUARDAR");
        actualizar();
    }
	public void cargar(){
		encargado=null;
		cambioEstado = false;
		Platform.runLater(()->{
			iv_Imagen.setImage( imgGuardado);
			lb_Texto.setText("GUARDADO");
			iv_Advertencia.setVisible(false);
			lb_Advertencia.setVisible(false);
		});
		
		actualizar();
	}

	@FXML
	private void evt_Encargado(MouseEvent event) {
		FXMLLoader loader = ScreenHandler.getLoader( "EmergentePersona" );
		ScreenHandler.showPopUp(loader);
		encargado= ( ( EmergentePersonaController )loader.getController() ).persona;
		tf_Encargado.setText(encargado != null ? encargado.getNombre()+" "+ encargado.getApellidos() : "");
	}

	@FXML
	private void evt_Eliminar(ActionEvent event) {
		if ( actDto != null )
         {
            try
            {
                ActividadService.eliminarActividad(actDto.getId() );
				proyecto.getActividades().remove(actDto);
                actDto = null;
                iv_Imagen.setImage( imgWarning );
                actualizar();
                Platform.runLater( () ->{
                lb_Advertencia.setText( "ELIMINADO" ); 
                iv_Advertencia.setVisible(true);
                iv_Advertencia.setImage(imgGuardado); 
				
				stage.close();
                });
				
            }catch ( ErrorInterno_Exception ex )
            {
                lb_Advertencia.setText( "ERROR INTERNO" );
                iv_Advertencia.setImage(imgWarning);
                iv_Advertencia.setVisible(true);
            }
            catch ( SinResultados_Exception ex )
            {
                lb_Advertencia.setText( "NO ENCONTRADO" );
                iv_Advertencia.setImage(imgWarning);
                iv_Advertencia.setVisible(true);
            }
            }else
            {
                lb_Advertencia.setText( "NO SE HA CARGADO SEGUIMIENTO" );
                iv_Advertencia.setImage(imgWarning);
                iv_Advertencia.setVisible(true);
            }
            lb_Advertencia.setVisible( true );
	}
    
}
