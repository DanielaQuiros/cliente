/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoredes.controller;

import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javax.imageio.ImageIO;
import proyectoredes.util.AppContext;
import proyectoredes.util.Bitacora;
import proyectoredes.util.Cliente;
import proyectoredes.util.FlowController;
import proyectoredes.util.Message;
import proyectoredes.util.Objeto;

/**
 * FXML Controller class
 *
 * @author Chaco
 */
public class RecibirController extends Controller implements Initializable {

    @FXML
    private JFXTextField tfPuerto;
    @FXML
    private Button bntRecibir;
    @FXML
    private AnchorPane apRoot;
    @FXML
    private VBox vBox;
    @FXML
    private JFXTextField tfIpServidor;
    
    Cliente cli = null;
    ArrayList<Bitacora> bitacora;

    /**
     * Initializes the controller class.
     */
    ImageView[][] mat;
    double f;
    double c;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @Override
    public void initialize() {
       f = 0;//Double.parseDouble((String)AppContext.getInstance().get("filas"));
       c = 0;
       bitacora= new ArrayList();
    }

    @FXML
    private void bntButton(ActionEvent event) throws IOException, InterruptedException {
        if(!tfPuerto.equals("") && !tfIpServidor.equals("")){
            cli = new Cliente(); 
            Bitacora br = (Bitacora)AppContext.getInstance().get("bit");
            if(!tfIpServidor.getText().equals(null)){
                br.ipOrigen = tfIpServidor.getText();
                bitacora.add(br);
                AppContext.getInstance().set("lista",bitacora);
            }
            cli.start();
            AppContext.getInstance().set("hiloCliente",cli);
        }else{
            new Message().showModal(Alert.AlertType.WARNING, "Información", this.getStage(), "Falta número de puerto y la Ip del servidor");
        }
        
        while(cli.getState()!=Thread.State.TERMINATED){}
        
        armarImagen();
        
    }
    
    public final void armarImagen() throws IOException{
        List<Objeto> objetos = (List<Objeto>) AppContext.getInstance().get("listaCliente");
        
        double w = 0;// =  cli.getObjeto().get(0)et//image.getWidth();
        double h = 0;// =  image.getHeight();
        
        System.out.println("armar imagen");
        
        //Double.parseDouble((String)AppContext.getInstance().get("columnas"));
        
        for(Objeto obj: objetos){
            if(obj.getNumeroSegmento() == 0){
                f = obj.getFila();
                c = obj.getColumna();
                obj.setFila(0);
                obj.setColumna(0);
            }
        }

        System.out.println("f "+(int)f +" c "+(int) c);
        
        mat = new ImageView[(int)f][(int)c]; 
        
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(0, 0, 0, 0));
        Image nul = new Image("/proyectoredes/resources/negro.jpg");
        
        int ancho = (int) (390/c); //columnas
        int alto = (int) (351/f); //filas

        for(double i = 0; i<f; i++){
            for(double j = 0; j<c; j++){
                //BufferedImage bimage = cli.getOnjeto().getImage();
                //System.out.println("i"+(int)i +" j "+(int) j +  " x " + x +" y "+ y );
                for(Objeto objeto: objetos){
                    if(objeto.getFila() == (int)i && objeto.getColumna() == (int) j){
                        //System.out.println("Cliente i "+i+" j "+j);
                        Image im = SwingFXUtils.toFXImage(objeto.getImage(),null);
                        ImageView iv = new ImageView();

                        if(!objeto.isError()){
                            iv.setImage(im);
                            iv.setVisible(false);
                        }else{
                            iv.setImage(nul);
                            iv.setVisible(false);
                        }
                        
                        mat[(int)i][(int)j] = iv;

                        iv.setFitHeight(alto);       //tamano de la imagen
                        iv.setFitWidth(ancho);        //tamano de la imagen

                        gridPane.add(iv, (int)j, (int)i);
                }
                }
            }
        }
        
        vBox.getChildren().add(gridPane);
        
        Runnable task = ()->{
            try {
                cargarImagen();
            } catch (InterruptedException ex) {
                Logger.getLogger(RecibirController.class.getName()).log(Level.SEVERE, null, ex);
            }
            }; 
		new Thread(task).start();
    }
    
    public void cargarImagen() throws InterruptedException{

        for(int i=0; i<f; i++) //filas y columnas
        {
            for(int j=0; j<c; j++)
            {
                System.out.println("Cliente i "+i+" j "+j);
                mat[i][j].setVisible(true);
                Thread.sleep((long) 100);
            }
        }
        
    }
    
}
